﻿# Windows launcher script for yspaceship
# author: Viktoras Veitas vveitas@gmail.com


if ($args[0] -eq "start") {
    $PDFLIBRARY_DEFAULT = Get-Content pdflibrary.conf | Out-String
    $PDFLIBRARY_DEFAULT=$PDFLIBRARY_DEFAULT.Trim()
    $PDFLIBRARY = Read-Host 'Full PATH to pdf library in a form C:\path\to\library [default - ' $PDFLIBRARY_DEFAULT']'
    if ($PDFLIBRARY -eq "") { $PDFLIBRARY = $PDFLIBRARY_DEFAULT }
    if ($PDFLIBRARY -eq $null) { $PDFLIBRARY = $PDFLIBRARY_DEFAULT }
    $PDFLIBRARY | Out-File -Encoding "utf8" -FilePath "pdflibrary.conf" 
  

    if (-not (Test-Path "spacegraph")) {
	    New-Item -ItemType directory "spacegraph"
        cd spacegraph
	    New-Item -ItemType directory "logs"
        New-Item -ItemType directory "configs"
        New-Item -ItemType directory "resources"
        $LIB = "{ 'pdf_library_dir_local': ''}"
        $EMPTY_JSON = "{}"
	    $LIB | Out-File -Encoding "utf8" -FilePath "configs\pbrain.conf"  
	    $EMPTY_JSON | Out-File -Encoding "utf8" -FilePath "resources\spaceGraphDataLast.dat"
	    $EMPTY_JSON | Out-File -Encoding "utf8" -FilePath "resources\enableFiltersTemp.dat"
	    cd ..
    }
    if (-not (Test-Path spacegraph\elasticsearch)) {
	    New-Item -ItemType directory spacegraph\elasticsearch
    }

    $CONF=Get-Content spacegraph\configs\pbrain.conf | ConvertFrom-Json
    $TEMP = $PDFLIBRARY
    $CONF.pdf_library_dir_local=$TEMP.replace("\", "\\")
#    $TEMP = $CONF.local_pdf_editor
#    $CONF.local_pdf_editor=$TEMP.replace("\", "\\")
    Write-Host $PDFLIBRARY
    (ConvertTo-Json $CONF) | Out-File -Encoding "utf8" -FilePath "spacegraph\configs\pbrain.conf"
    
    

    $temp= Invoke-WebRequest 'https://registry.hub.docker.com/v2/repositories/vveitas/yspaceship/tags' | ConvertFrom-Json
    $LATEST_REMOTE= $temp.results.name[0]
    $LATEST_LOCAL= ((docker images vveitas/yspaceship)[1]).ForEach({($_ -split '\s+')[1]}) -match '\d'

    if ($LATEST_REMOTE -ge $LATEST_LOCAL) {
       $RUN_TAG=$LATEST_REMOTE
    } Else {
       $RUN_TAG=$LATEST_LOCAL
    }
    Write-Host "Run tag: "$RUN_TAG

    $LOCAL_EXISTS=docker images vveitas/yspaceship:$RUN_TAG -q
    if ($LOCAL_EXISTS -eq "") {
        Write-Host -NoNewline "New ySpaceShip version " $RUN_TAG "is available"
        Write-Host ""
        $UPDATE = Read-Host "Update/download? [y/N]"
        if (($UPDATE -eq "y") -or ($UPDATE -eq "Y")) {
           docker pull vveitas/yspaceship:$RUN_TAG
        }
    }

    $SCRIPT_PATH = $PSScriptRoot
    $CONTAINER_NAME= -join ((1..16) | %{(65..90)+(97..122) | Get-Random} | % {[char]$_})
    $CONTAINER_NAME | Out-File -Encoding "utf8" -FilePath "container.name"

    if ($args[1] -eq "debug") {
        Write-Host "running yspacheship in debug mode"
        docker run --name $CONTAINER_NAME -i -t -p 3000:3000 -p 3001:3001 -p 3002:3002 -p 8182:8182 -v $SCRIPT_PATH/spacegraph:/opt/yspaceship/spacegraph -v ${PDFLIBRARY}:/opt/yspaceship/pdflibrary:ro vveitas/yspaceship:$RUN_TAG /bin/bash
    }
    Else {
        Write-Host "Launching ySpaceShip..."
        docker run --name $CONTAINER_NAME -d -p 3000:3000 -p 3001:3001 -p 3002:3002 -p 8182:8182 -v $SCRIPT_PATH/spacegraph:/opt/yspaceship/spacegraph -v ${PDFLIBRARY}:/opt/yspaceship/pdflibrary:ro vveitas/yspaceship:$RUN_TAG sh ./scripts/shell/yspaceship.sh start
        Write-Host "...Done."
        Start-Sleep -s 2
    }

}

if ($args[0] -eq "stop") {
    if (Test-Path "container.name") {
        $CONTAINER_NAME = Get-Content container.name
        Write-Host "Stopping ySpaceShip.."
        docker stop $CONTAINER_NAME
        rm container.name
        Write-Host "...Done."
        Start-Sleep -s 1
    }
    Else {
        Write-Host "yspaceship container is not running... nothing to stop. if that is not the case, then the container name was lost on the way and you have to stop it manually..."
    }
}
