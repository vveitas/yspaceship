#!/bin/bash

# copy required files into docker build directory (otherwise fails...)
cp -r /opt/yspaceship/required .

# building docker image
docker-compose build

#squashing image (does not seem to have any effect...)
#docker save vveitas/yspaceship > image.tar
#sudo docker-squash -i image.tar -o squashed.tar -t 0.0.1
#cat squashed.tar | docker load
