#!/bin/bash

if [ "$1" == "start" ]; then

  read PDFLIBRARY_DEFAULT < pdflibrary.conf;
  read -p "Full PATH to pdf library in a form /path/to/library [$PDFLIBRARY_DEFAULT]" PDFLIBRARY
  PDFLIBRARY=${PDFLIBRARY:-$PDFLIBRARY_DEFAULT}
#  echo $name
#  read -e -p "Full PATH to pdf library in a form /path/to/library " -i $PDFLIBRARY PDFLIBRARY;
  echo $PDFLIBRARY > pdflibrary.conf;

  if [ ! -d spacegraph ]; then
    mkdir -p spacegraph/logs;
    mkdir -p spacegraph/configs;
    mkdir -p spacegraph/resources;
    echo '{}' > ./spacegraph/configs/pbrain.conf;
    chmod a+rw ./spacegraph/configs/pbrain.conf;
    echo "{}" > ./spacegraph/resources/spaceGraphDataLast.dat;
    chmod a+rw ./spacegraph/resources/spaceGraphDataLast.dat;
    echo "{}" > ./spacegraph/resources/applyPublicationsFilterLast.dat;
    chmod a+rw ./spacegraph/resources/applyPublicationsFilterLast.dat;
    echo "{}" > ./spacegraph/resources/applyApplicationsFilterLast.dat;
    chmod a+rw ./spacegraph/resources/applyApplicationsFilterLast.dat;
   fi
  if [ ! -d spacegraph/elasticsearch ]; then
    mkdir -p spacegraph/elasticsearch;
  fi

  echo $PDFLIBRARY
  cat ./spacegraph/configs/pbrain.conf | jq --arg pdflibrary "$PDFLIBRARY" '.pdf_library_dir_local = $pdflibrary' > ./spacegraph/configs/pbrain2.conf
  rm ./spacegraph/configs/pbrain.conf; mv ./spacegraph/configs/pbrain2.conf ./spacegraph/configs/pbrain.conf

  LATEST_REMOTE_TEMP=$(curl -s -S 'https://registry.hub.docker.com/v2/repositories/vveitas/yspaceship/tags/' | jq '."results"[]["name"]' | head -n1)
  LATEST_REMOTE=$(sed -e 's/^"//' -e 's/"$//' <<< $LATEST_REMOTE_TEMP)
  LATEST_LOCAL=$(docker images vveitas/yspaceship --format {{.Tag}} | sort -r | head -n 1)

  LATEST_REMOTE_NUMBER=$(sed -e 's/[.]//g' -e 's/[-]//g' -e 's/^0*//' <<< $LATEST_REMOTE)
  LATEST_LOCAL_NUMBER=$(sed -e 's/[.]//g' -e 's/[-]//g' -e 's/^0*//' <<< $LATEST_LOCAL)
  if [ $LATEST_REMOTE_NUMBER -gt $LATEST_LOCAL_NUMBER ]; then
    RUN_TAG=$LATEST_REMOTE;
  else
    RUN_TAG=$LATEST_LOCAL;
  fi;

  if [ "$(docker images vveitas/yspaceship:$RUN_TAG -q 2>/dev/null)" == "" ]; then
    echo "New ySpaceShip version $RUN_TAG is available";
    read -r -p "Update/download? [y/N] " response
    response=${response,,}    # tolower
    if [[ $response =~ ^(yes|y)$ ]]; then
      docker rmi -f vveitas/yspaceship;
    fi
  fi

  SCRIPT_PATH=$(cd "$(dirname "$0")"; pwd);
  CONTAINERNAME=$(cat /dev/urandom | env LC_CTYPE=C tr -dc a-z0-9 | head -c 16; echo);
  echo $CONTAINERNAME > container.name;
  if [ "$2" == "debug" ]; then
    echo 'running yspaceship in debug mode';
    docker run --name $CONTAINERNAME -i -t -p 3000:3000 -p 3001:3001 -p 3002:3002 -p 8182:8182 -v $SCRIPT_PATH/spacegraph:/opt/yspaceship/spacegraph -v $PDFLIBRARY:/opt/yspaceship/pdflibrary:ro vveitas/yspaceship:$RUN_TAG /bin/bash;
  else
    docker run --name $CONTAINERNAME -d -p 3000:3000 -p 3001:3001 -p 3002:3002 -p 8182:8182 -v $SCRIPT_PATH/spacegraph:/opt/yspaceship/spacegraph -v $PDFLIBRARY:/opt/yspaceship/pdflibrary:ro vveitas/yspaceship:$RUN_TAG sh scripts/shell/yspaceship.sh start;
    until [ "`docker inspect -f {{.State.Running}} $CONTAINERNAME`" == "true" ]; do
      sleep 5;
    done;
    #sensible-browser http://localhost:3000;
  fi
fi
if [ "$1" == "stop" ]; then
  if [ -f container.name ]; then
    read CONTAINERNAME < container.name;
#     docker commit $CONTAINERNAME vveitas/yspaceship;
    docker stop $CONTAINERNAME;
  else
    echo 'yspaceship container is not running... nothing to stop. if that is not the case, then the container name was lost on the way and you have to stop it manually...';
  fi
fi
