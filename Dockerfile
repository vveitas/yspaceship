FROM ubuntu:16.04
MAINTAINER Viktoras Veitas "vveitas@gmail.com"

# install required packages
RUN apt-get update && apt-get -y install curl python2.7 python-poppler-qt4

# create application directory in the container and copy app
RUN mkdir -p /opt/yspaceship/required
COPY lss /opt/yspaceship/lss 
COPY scripts /opt/yspaceship/scripts 

# add all required dependencies to where they belong
WORKDIR /opt/yspaceship
COPY required /opt/yspaceship/required

# install node
RUN ln -s /opt/yspaceship/required/node/bin/node /usr/local/bin/node && ln -s /opt/yspaceship/required/node/bin/npm /usr/local/bin/npm

# adding required links and directories

# for titan:
RUN cd /opt/yspaceship/required/titan-server-0.4.4/conf && rm cassandra.yaml && rm rexster-cassandra-es.xml && ln -s ../../../lss/configs/cassandra.yaml cassandra.yaml && ln -s ../../../lss/configs/rexster-cassandra-es.xml rexster-cassandra-es.xml

# for spacegraph
RUN mkdir -p /opt/yspaceship/spacegraph/elasticsearch

# for elasticsearch
RUN mkdir -p /opt/yspaceship/required/elasticsearch-0.90.3/data && ln -s /opt/yspaceship/spacegraph/elasticsearch /opt/yspaceship/required/elasticsearch-0.90.3/data/elasticsearch

# for the bash
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# for writing log and some files outside the docker container
RUN ln -sfn /opt/yspaceship/spacegraph/logs /opt/yspaceship/lss/ && ln -sfn /opt/yspaceship/spacegraph/configs/pbrain.conf /opt/yspaceship/lss/configs/pbrain.conf && ln -sfn /opt/yspaceship/spacegraph/resources /opt/yspaceship/lss/

# for the jps
RUN ln -s /opt/yspaceship/required/java-8-openjdk-amd64/bin/jps /usr/bin/jps

# add required executables to PATH
RUN echo 'export JAVA_HOME=/opt/yspaceship/required/jre' >> /root/.bashrc && echo 'export GROOVY_HOME=/opt/yspaceship/required/groovy' >> /root/.bashrc & echo 'export GROOVY_LIB=/opt/yspaceship/required/groovy_lib' >> /root/.bashrc && echo 'export PATH=$PATH:$JAVA_HOME/bin:$GROOVY_HOME/bin:' >> /root/.bashrc

# include all downloaded libraries into java classpath, so that less duplication is needed
RUN echo 'export CLASSPATH=$JAVA_HOME/lib:$GROOVY_HOME/lib:' >> /root/.bashrc

WORKDIR /opt/yspaceship/lss

# import all required classes via Grapes (to speed up first run)
RUN sh scripts/shell/importClasses.sh

# install what is needed for python to run (THIS ONE IS VERY LARGE - NEED TO OPTIMIZE):
RUN apt-get install python-pip netcat -y && pip install ftfy==4.4.3 && apt-get remove --purge python-pip -y

# build application
RUN npm install

# ports
EXPOSE 3000 3001 3002 8182
