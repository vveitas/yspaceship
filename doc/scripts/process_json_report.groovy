import groovy.json.JsonSlurper
import groovy.json.JsonOutput

wd='_temp/'
def json_report_processed = new JsonSlurper().parse(new File(wd+'cucumber-json-report-processed.json').toURL())

json_report_processed.each { feature ->
	def includeFeature = (feature.tags.find {tag->tag.name=='@indoc'} ? true : false)
	if (includeFeature && feature.result.status =="passed") {
		println('## '+feature.name)
		println(feature.description)
		feature.elements.each {  scenario->
			def includeScenario = (scenario.tags.find {tag->tag.name=='@indoc'} ? true : false)
			if (includeScenario && scenario.result.status =="passed") {
				println('### '+scenario.keyword+': '+scenario.name)
				println(scenario.description)
				scenario.steps.each { step ->
					println('* '+step.keyword+': '+step.name+'\n')
				}
				println("![](images/"+scenario.name.replaceAll(' ','_')+".gif)")
			}
		}
	}
}