#!/bin/bash

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH

groovy scripts/export_png_files.groovy