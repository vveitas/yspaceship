#!/bin/sh

WD='./_temp/screenshots/'
cd $WD
for DIR in */
do
  ANIMATED=$(basename $DIR)
  convert -delay 140 -loop 0 ../../img/start_page.png $DIR*.png $DIR/../$ANIMATED.gif  
done
