#!/bin/bash

DISPLAY_NUM=0
TMP=0
while [ $TMP -eq 0 ]; do
  if [ -f /tmp/.X$DISPLAY_NUM-lock ]; then
    DISPLAY_NUM=$((DISPLAY_NUM+1))
  else
    echo $DISPLAY_NUM    
    TMP=1
  fi;
done
