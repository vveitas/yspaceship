#!/bin/sh

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH

groovy -cp scripts/groovy scripts/groovy/build-test-report.groovy

exit 0
