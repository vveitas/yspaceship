node {
 timestamps {
   try {
    bitbucketStatusNotify(
        buildState: 'INPROGRESS',
    )

     stage('Checkout repository') {
        checkout scm
     }

     stage('Build Assets') {
          dir(env.WORKSPACE) {
            sh 'scripts/assets.sh'
          }
     }

     stage('Import required classes') {
          dir(env.WORKSPACE+'/lss') {
              sh 'scripts/shell/importClasses.sh'
          }
     }
     stage('Compile Groovy to Java') {
          dir(env.WORKSPACE) {
              sh 'scripts/compileGroovy.sh'
          }
     }

     stage('Build Docker image') {
          dir(env.WORKSPACE) {
            //sh 'scripts/container-cleanup.sh'
            //sh 'scripts/image-cleanup.sh'
            sh 'docker info'
            sh 'scripts/docker-image.sh'
            sh 'docker images'
          }
      }

     stage('Run docker container locally') {
          dir(env.WORKSPACE+"/test") {
            sh 'scripts/shell/yspaceship-test-setup.sh'
          }
          dir(env.WORKSPACE+"/test/deployment") {
            // stop any running docker container before
            // this will work only if the whole machine is dedicated to yspaceship testing
            // otherwise it will start to break things...
            // sh 'docker stop $(docker ps -q)'
            // midify this to check if anything is running (even better -use docker plugin)
            sh 'yes "" | ./yspaceship.sh start'
            sleep 5
            sh 'docker ps'
          }
      }


     stage('Run tests') {
        dir(env.WORKSPACE+"/test") {
          env.PATH = "/sbin:${env.PATH}" // needed for start-stop-daemon
          env.DISPLAY = ":99" // for the xvfb
          sh 'echo $DISPLAY'
          sh 'scripts/shell/xvfb.sh start 99'
          try {
            sh './gradlew test'
          } catch (err) {
            currentBuild.result = "FAILED"
          } finally {
            sh 'scripts/shell/xvfb.sh stop'
            sh 'cd deployment; sh ./yspaceship.sh stop; cd ..'
            sh 'scripts/shell/build-test-report.sh'
            sh 'cp -r target/cucumber-json-report.json $JENKINS_HOME/jobs/$JOB_NAME/builds/$BUILD_ID/'
            publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'target/cucumber-html-reports', reportFiles: 'overview-features.html', reportName: 'Cucumber-JVM-Report'])
          }
        }
    }

    stage('Compile documentation') {
      dir(env.WORKSPACE) {
        sh 'mkdir -p doc/_temp'
        sh 'cp -r test/target/cucumber-json-report.json doc/_temp/'
      }
      dir(env.WORKSPACE+"/doc") {
        sh 'scripts/doc_knit.sh'
        publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: '_knitted/_book', reportFiles: 'index.html', reportName: 'ySpaceShip documentation'])
      }
    }

    stage('Assemble logs and cleanup') {
          dir(env.WORKSPACE+"/test/deployment") {
            publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: true, reportDir: 'spacegraph/logs', reportFiles: 'nodejs-output.log, rolling.log', reportName: 'Log Files'])
            sh 'cp -r spacegraph/logs $JENKINS_HOME/jobs/$JOB_NAME/builds/$BUILD_ID/'
            sh 'sudo rm -r *'
          }
         deleteDir()
     }

      bitbucketStatusNotify(
          buildState: 'SUCCESS',
      )

    } catch(Exception) {
      bitbucketStatusNotify(
       buildState: 'FAILED',
       buildDescription: 'Something went wrong with build!'
      )
    }
 }
}
