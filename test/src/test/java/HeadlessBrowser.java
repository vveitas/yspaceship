package yspaceship.test;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

public class HeadlessBrowser {
    static Chrome browser = new Chrome();
    //static HeadlessChrome browser = new HeadlessChrome();
    public static WebDriver driver = browser.driver;
    public static JavascriptExecutor js = browser.js;

    static String SCRIPT_GET_ELEMENT_BORDER = browser.SCRIPT_GET_ELEMENT_BORDER;
    static String SCRIPT_UNHIGHLIGHT_ELEMENT = browser.SCRIPT_UNHIGHLIGHT_ELEMENT;

    public void ass() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                close();
            }
          });
    }

    public static String highlightElement(WebElement elem) {
        String lastBorder = (String)(js.executeScript(SCRIPT_GET_ELEMENT_BORDER, elem));
        return lastBorder;
    }

    public static void unhighlightLast(WebElement lastElem, String lastBorder) {
        if (lastElem != null) {
            try {
                // if there already is a highlighted element, unhighlight it
                js.executeScript(SCRIPT_UNHIGHLIGHT_ELEMENT, lastElem, lastBorder);
            } catch (StaleElementReferenceException ignored) {
                // the page got reloaded, the element isn't there
            } finally {
                // element either restored or wasn't valid, nullify in both cases
                lastElem = null;
            }
        }
    }     
    
    public static void close() {
        driver.close();
        driver.quit();
        Utils.runXvfbScript(new String[]{"stop"});
    }   

}