package yspaceship.test;

import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.After;
import cucumber.api.java.Before;


import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

import java.io.File;
import org.apache.commons.io.FileUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import com.google.common.collect.ImmutableMap;

import yspaceship.test.HeadlessBrowser;

public class ApplicationManagementWindowsStepDefs {
    WebDriver driver = HeadlessBrowser.driver;
    private JavascriptExecutor js = HeadlessBrowser.js;

    private Scenario thisScenario;
    private WebElement lastElem = null;
    private String lastBorder = null;

    @Before
    public void before(Scenario scenario) throws Throwable {
        this.thisScenario = scenario;
        this.driver.get("http://localhost:3000");
    }
    
    /*
    * Feature: Application management    
    */
    /*
    * Scenario: open application management menu
    */

    @Given("^the ySpaceShip is running$") 
    public void the_yspaceship_is_running() throws Throwable {
        String actualTitle = driver.getTitle();
        String expectedTitle = "ySpaceShip";
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertEquals(expectedTitle,actualTitle);        
    }

    @When("^user presses gear icon$")
    public void user_presses_gear_icon() throws Throwable {
        WebElement elem = driver.findElement(By.id("management"));
        lastElem=elem; lastBorder = HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();
    }

    @Then("^application management menu is opened$")
    public void application_management_menu_is_opened() throws Throwable {
        WebElement management = driver.findElement(By.id("management"));
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertTrue("Application management menu is not opened",management.isDisplayed());           
    }

    /*
    * Scenario: open application settings dialogue windows
    */

    @Given("^the \"([^\"]*)\" menu is open$")
    public void the_menu_is_open(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        elem.click();
        assertTrue(String.format("%s is displayed",arg1),elem.isDisplayed());           
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
    }

    @When("^user chooses \"([^\"]*)\"$")
    public void user_chooses(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();    
    }

    @Then("^\"([^\"]*)\" is displayed$")
    public void is_displayed(String arg1) throws Throwable {
        driver.switchTo().activeElement();
        Thread.sleep(1000);
        WebElement elem = driver.findElement(By.id(arg1));
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertTrue(String.format("%s is displayed",arg1),elem.isDisplayed());           
    }

    /*
    * Scenario: toggle filters view
    */
    @When("^user presses \"([^\"]*)\" \\(eye\\) icon$")
    public void user_presses_eye_icon(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();    
    }

    @Then("^\"([^\"]*)\" is hidden$")
    public void is_hidden(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        Thread.sleep(1000);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertTrue(String.format("%s is displayed",arg1),!elem.isDisplayed());           
    }

    /*
    * Scenario: display help
    */

    @When("^user presses \"([^\"]*)\" icon$")
    public void user_presses_icon(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();    
    }

    @Then("^user guide opens at page \"([^\"]*)\" in separate browser tab$")
    public void user_guide_opens_at_page_in_separate_browser_tab(String arg1) throws Throwable {
        ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(1));
        Thread.sleep(1000);
        String actualTitle = driver.getTitle();
        String expectedTitle = arg1;
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertEquals(expectedTitle,actualTitle);      
        driver.close();
        driver.switchTo().window(tabs2.get(0));

    }


}
