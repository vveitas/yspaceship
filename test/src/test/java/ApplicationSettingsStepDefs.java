package yspaceship.test;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.Set;


import cucumber.api.PendingException;
import cucumber.api.Scenario;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import cucumber.api.java.After;
import cucumber.api.java.Before;


import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import org.apache.commons.io.FileUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import com.google.common.collect.ImmutableMap;

import yspaceship.test.HeadlessBrowser;

import groovy.json.JsonSlurper;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.lang.Object;

public class ApplicationSettingsStepDefs {
    WebDriver driver = HeadlessBrowser.driver;
    private JavascriptExecutor js = HeadlessBrowser.js;

    private Scenario thisScenario;
    private WebElement lastElem = null;
    private String lastBorder = null;

    @Before
    public void before(Scenario scenario) throws Throwable {
        this.thisScenario = scenario;
        this.driver.get("http://localhost:3000");

        /*
        * Need to add clearing publications filter (= resetting the whole application to the initial state) before each scenario so that scenarios are not depend on each other
        below is the snippet from javascript -- need to repeat the same there (but it is quite complicated in java... for no)
        
        function clearPublicationsFilter() {
           $.post("/clearPublicationsFilter", function() {
              socket.emit('setActivePane', {
                publicationsPaneStatus: 'active',
                annotationsPaneStatus: '',
                publicationsViewStatus: 'in active',
                annotationsViewStatus: ''
              });
          location.reload();
        });
    */

    }

    /*
    * Feature: Application settings
    */

    @Given("^\"([^\"]*)\" dialogue is displayed$")
    public void dialogue_is_displayed(String arg1) throws Throwable {
        driver.findElement(By.id("management")).click();
        driver.findElement(By.id(arg1+"-menu-item")).click();
        Thread.sleep(1000);
        WebElement elem = driver.findElement(By.id(arg1));
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertTrue(String.format("%s is displayed",arg1),elem.isDisplayed());
    }

    /*
    * Scenario Outline: Input and save settings with defaults
    */

    @When("^user inputs settings '(.+)', '(.+)' and '(.+)'$")
    public void user_inputs_zotero_library_settings(String zotero_key, String zotero_userID, String zotero_collectionID) throws Throwable {
        driver.findElement(By.id("zotero_key")).clear();
        driver.findElement(By.id("zotero_key")).sendKeys(zotero_key);
        driver.findElement(By.id("zotero_userID")).clear();
        driver.findElement(By.id("zotero_userID")).sendKeys(zotero_userID);
        driver.findElement(By.id("zotero_collectionID")).clear();
        driver.findElement(By.id("zotero_collectionID")).sendKeys(zotero_collectionID);
    }

    @When("^user presses \"([^\"]*)\" button$")
    public void presses_save_button(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();
        Thread.sleep(500);
    }

    @Then("^\"([^\"]*)\" dialog closes$")
    public void dialog_closes(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        Thread.sleep(1000);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertTrue(String.format("%s is displayed",arg1),!elem.isDisplayed());
     }


    @Then("^file <pbrain\\.conf> contains settings '(.+)', '(.+)' and '(.+)'$")
    public void file_pbrain_conf_contains_application_and_default_settings(String zotero_key, String zotero_userID, String zotero_collectionID) throws Throwable {
        Map config = (Map) new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/deployment/spacegraph/configs/pbrain.conf"));
        assertEquals(zotero_key,config.get("zotero_key"));
        assertEquals(zotero_userID,config.get("zotero_userID"));
        assertEquals(zotero_collectionID,config.get("zotero_collectionID"));
    }

    @When("^user selects \"([^\"]*)\" as '(.+)'$")
    public void user_selects_as_PDF_XChange_Windows(String id, String pdfEditor) throws Throwable {
        Select dropdown = new Select(driver.findElement(By.id(id)));
        dropdown.selectByVisibleText(pdfEditor);
    }

    @Then("^file \"([^\"]*)\" has \"([^\"]*)\" value '(.+)'$")
    public void file_has_value_PDF_XChange_Windows(String confFile, String id, String pdfEditor) throws Throwable {
        Map config = (Map) new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/deployment/spacegraph/configs/pbrain.conf"));
        assertEquals(pdfEditor,config.get(id));
    }

    /*
    * Feature: Control Room
    */

    /*
    * Scenario: Clear SpaceGraph Database
    */
    @When("^user presses \"([^\"]*)\" button and waits for script to finish$")
    public void presses_button_and_waits_for_script_to_finish(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();
        switch (arg1) {
            case "clearSpaceGraph":
                int timeout = 60;
                long timeStart = System.currentTimeMillis();
                WebDriverWait wait = new WebDriverWait(driver, timeout);
                String expectedText = "SpaceGraph is empty..";
                wait.until(ExpectedConditions.textToBePresentInElement(elem,expectedText));
                // waits until the text of element changes (to figure out when script changes for certain buttons)
                thisScenario.write("clearSpaceGraph script run time in seconds: "+(System.currentTimeMillis()-timeStart)/1000);
                break;
            case "synchronizeSpaceGraph":
                timeout = 60;
                timeStart = System.currentTimeMillis();
                wait = new WebDriverWait(driver, timeout);
                expectedText = "Synchronized";
                wait.until(ExpectedConditions.textToBePresentInElement(elem,expectedText));
                // waits until the text of element changes (to figure out when script changes for certain buttons)
                thisScenario.write("synchronizeSpaceGraph script run time in seconds: "+(System.currentTimeMillis()-timeStart)/1000);
                break;
            default:
                Thread.sleep(500);
                break;
        }
    }

    @Then("^\"([^\"]*)\" button text is \"([^\"]*)\"$")
    public void button_text_is(String buttonID, String expectedText) throws Throwable {
        WebElement elem = driver.findElement(By.id(buttonID));
        assertEquals(expectedText,elem.getText());
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
    }


    @And("^the spacegraph is cleared of all contents$")
    public void the_spacegraph_is_cleared_of_all_contents() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // connect to space graph and test if it is empty:
        String query = "http://localhost:8182/graphs/graph/vertices";
        URL url = new URL(query);
        URLConnection uc = url.openConnection();

        uc.setRequestProperty("X-Requested-With", "Curl");
        InputStreamReader inputStreamReader = new InputStreamReader(uc.getInputStream());
        Object json = new JsonSlurper().parse(inputStreamReader);
        Map jsonResult = (Map) json;
        int totalSize = (int) jsonResult.get("totalSize");
        assertEquals("SpaceGraph was not properly cleaned -- contains more than one element",0, totalSize);

    }

    /*
    *  Scenario: Synchronize graph with no settings - unsucessfull
    */

    @Given("^file <pbrain\\.conf> does not contain valid settings$")
    public void file_pbrain_conf_does_not_contain_valid_settings() throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^\"([^\"]*)\" dialog is displayed$")
    public void dialog_is_displayed(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    /*
    * Scenario Outline: initiate individual synchronization steps
    */

    @Given("^file <pbrain\\.conf> contains valid settings:$")
    public void file_pbrain_conf_contains_valid_settings(DataTable settingsTable) throws Throwable {
        Map config = (Map) new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/deployment/spacegraph/configs/pbrain.conf"));
        Map settingsMap = settingsTable.asMap(String.class,String.class);
        Set keys = settingsMap.keySet();
        for (Object keyObj : keys ) {
            String key = (String) keyObj;
            assertEquals("zotero_key is not valid",config.get(key),settingsMap.get(key));
        }
    }

    @When("^user presses '(.+)' button for starting '(.+)'$")
    public void presses_id_zotero_synchronization_div_button_button_for_starting_zotero_synchronization(String path, String stepName) throws Throwable {
        WebElement elem = driver.findElement(By.xpath(path));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();

        long timeStart = System.currentTimeMillis();
        boolean wait = true;
        int timeout = 3600000; // setting timeout for one hour -- large syncrhonization scripts can run like that;
        while (wait) {
            Thread.sleep(1000);
            WebElement parent = elem.findElement(By.xpath("../.."));
            String elementClass = parent.getAttribute("class");
            if (elementClass.contains("finished")) wait=false;
            assertTrue("Timeout reached without result",System.currentTimeMillis()-timeStart<timeout);
        }
        //wait.until(ExpectedConditions.attributeContains(elem,"class","finished"));
        thisScenario.write(stepName+" script run time in seconds: "+(System.currentTimeMillis()-timeStart)/1000);

    }

    @Then("^element '(.+)' has text: \"([^\"]*)\"$")
    public void id_zotero_synchronization_class_indicator_has_text(String path, String expectedText) throws Throwable {
        WebElement elem = driver.findElement(By.xpath(path));
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        assertEquals(path+" does not contain expectedText",expectedText, elem.getText());
    }

    @Then("^element '(.+)' has attribute \"([^\"]*)\": \"([^\"]*)\"$")
    public void id_zotero_synchronization_class_indicator_has_class(String path, String attribute, String expectedClass) throws Throwable {
        WebElement elem = driver.findElement(By.xpath(path));
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        String attributeValue = elem.getAttribute(attribute);
        assertTrue(path+" does not contain "+expectedClass,attributeValue.contains(expectedClass));
    }

    /*
    * Feature: Publication filters
    */

    /*
    * Scenario Outline: Full text query returns both publication and annotation
    */
    @When("^user writes '(.+)' to \"([^\"]*)\" input field$")
    public void user_writes_queryString_to_input_field(String queryString, String elementID) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementID));
        elem.clear();
        elem.sendKeys(queryString);
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
    }

    @When("^user presses \"([^\"]*)\" button and waits for no more than \"([^\"]*)\" seconds$")
    public void user_presses_button_and_waits_for_no_more_than_seconds(String elementID, int timeout) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementID));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollTo(0,0)", "");
        elem.click();
        Thread.sleep(timeout*1000);
    }


    @Then("^\"([^\"]*)\" contains \"([^\"]*)\" row having \"([^\"]*)\" with '(.+)'$")
    public void contains_with_queryString(String tableID, String rowNumber, String columnClass, String queryString) throws Throwable {
        String path = "//*[@id='"+tableID+"']/tbody/tr["+rowNumber+"]/td[contains(@class,'"+columnClass+"')]";
        WebElement elem = driver.findElement(By.xpath(path));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        String actualText = elem.getText();
        assertTrue(actualText.toLowerCase().contains(queryString.toLowerCase()));
    }

    @Then("^\"([^\"]*)\" contains '(.+)' rows of class \"([^\"]*)\"$")
      public void contains_rows_with_quaryString(String tableID, String expectedRowNumber, String columnClass) throws Throwable {
          String path = "//*[@id='"+tableID+"']/tbody/tr[contains(@class,'"+columnClass+"')]";
          //String path = "//*[@id='"+tableID+"']/tbody/tr["+rowNumber+"]/td[contains(@class,'"+columnClass+"')]";
          List<WebElement> rows = driver.findElements(By.xpath(path));
          byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
          thisScenario.embed(screenshot, "image/png");
          assertEquals(Integer.parseInt(expectedRowNumber),rows.size());
      }

    /*
    * Scenario: "Filters enabled" mode
    */

    @When("^user writes \"([^\"]*)\" to \"([^\"]*)\" input field$")
    public void user_writes_a_string_to_input_field(String queryString, String elementID) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementID));
        elem.clear();
        elem.sendKeys(queryString);
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
    }

    @Then("^\"([^\"]*)\" with \"([^\"]*)\" is displayed$")
    public void with_is_displayed(String elementID, String elementText) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementID));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        assertTrue(String.format("%s is displayed",elementID),elem.isDisplayed());
    }

    @Then("^\"([^\"]*)\" with \"([^\"]*)\" is NOT displayed$")
    public void with_is_not_displayed(String elementID, String elementText) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementID));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        assertTrue(String.format("%s is displayed",elementID),!elem.isDisplayed());
    }

    /*
    * Scenario Outline: Filter by tag / author /publication
    */

    @When("^user checks '(.+)' in \"([^\"]*)\"$")
    public void user_checks_a_checkbox(String tagName, String listName) throws Throwable {
        String path = "//*[@id='"+listName+"']/tbody/tr/td[normalize-space(text())='"+tagName+"']/../td[1]/input";
        WebElement elem = driver.findElement(By.xpath(path));
        elem.click();
        path = "//*[@id='"+listName+"']/tbody/tr/td[normalize-space(text())='"+tagName+"']/..";
        elem = driver.findElement(By.xpath(path));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);

    }

    @Then("^\"([^\"]*)\" contains '(.+)'$")
    public void contains(String tableName, int expectedRowNumber) throws Throwable {
        String path = "//*[@id='"+tableName+"']/tbody/tr";
        WebElement elem = driver.findElement(By.id(tableName));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        List<WebElement> rows = driver.findElements(By.xpath(path));
        assertEquals(expectedRowNumber,rows.size());
    }


    /*
    * Scenario Outline: combined filters
    */


    @When("^user selects '(.+)' operator$")
    public void user_selects_operator(String operator) throws Throwable {
        String path = "//*[@id='search-operator']/../input";
        WebElement elem = driver.findElement(By.xpath(path));
        //WebElement elem = driver.findElement(By.id("search-operator"));
        if (operator.equals("OR")) {
            //elem.sendKeys(Keys.SPACE);
            elem.click();
            Thread.sleep(500);
            assertTrue(elem.isSelected());
            System.out.println("operator is "+operator+" therefore #search-operator checkbox is selected: "+elem.isSelected());
        } else { 
            assertFalse(elem.isSelected()); 
            System.out.println("operator is "+operator+" therefore #search-operator checkbox is NOT selected"+elem.isSelected());
        }
        path = "//*[@id='search-operator']/..";
        elem = driver.findElement(By.xpath(path));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
    }

    /*
    * Feature: Quick filters
    */

    /*
    * Scenario Outline: Quick filter on '<table>'
    */

    @When("^user presses '(.+)' button$")
    public void presses_save_button_outline(String arg1) throws Throwable {
        WebElement elem = driver.findElement(By.id(arg1));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();
        Thread.sleep(500);
    }

    @When("^user writes '(.+)' to '(.+)' input field$")
    public void user_writes_to_filter_input_field(String queryString, String elementID) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementID));
        elem.clear();
        elem.sendKeys(queryString);
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
    }

    @Then("^'(.+)' contains '(.+)' rows$")
    public void contains_outline(String tableName, int expectedRowNumber) throws Throwable {
        String path = "//*[@id='"+tableName+"']/tbody/tr[contains(@style,'display: table-row;')]";
        WebElement elem = driver.findElement(By.id(tableName));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        List<WebElement> rows = driver.findElements(By.xpath(path));
        assertEquals(expectedRowNumber,rows.size());
    }

    /*
    * Scenario: Helper feature: Toggle selecting all publications
    */

    @When("^all items in \"([^\"]*)\" are unchecked$")
    public void all_items_in_are_unchecked(String tableName) throws Throwable {
        String path = "//*[@id='"+tableName+"']/tbody/tr/td[@class='mark']/input";
        WebElement elem = driver.findElement(By.id(tableName));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        List<WebElement> checkboxes = driver.findElements(By.xpath(path));
        for (int i = 0;i<checkboxes.size();i++) {
            assertFalse(checkboxes.get(i).isSelected());
        }
    }

    @When("^user toggles \"([^\"]*)\"$")
    public void user_checks(String elementName) throws Throwable {
        WebElement elem = driver.findElement(By.id(elementName));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        elem.click();
        Thread.sleep(500);
    }

    @Then("^all items in \"([^\"]*)\" are checked$")
    public void all_items_in_are_checked(String tableName) throws Throwable {
        String path = "//*[@id='"+tableName+"']/tbody/tr/td[@class='mark']/input";
        WebElement elem = driver.findElement(By.id(tableName));
        lastElem=elem;lastBorder=HeadlessBrowser.highlightElement(elem);
        byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        thisScenario.embed(screenshot, "image/png");
        HeadlessBrowser.unhighlightLast(lastElem,lastBorder);
        List<WebElement> checkboxes = driver.findElements(By.xpath(path));
        for (int i = 0;i<checkboxes.size();i++) {
            assertTrue(checkboxes.get(i).isSelected());
        }
    }

}
