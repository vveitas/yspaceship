package yspaceship.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.ArrayList;

public class Utils {
    static String path = Utils.class.getProtectionDomain().getCodeSource().getLocation().getPath();

    static String getFreeDisplayNumber() throws Throwable {
        boolean foundFree=false;
        int displayNumber=0;
        String result="";
        while (!foundFree) {
            File lock = new File("/tmp/.X"+displayNumber+"-lock");
            if (lock.exists()) {
                displayNumber=+1;
            } else {
                result = Integer.toString(displayNumber);
                foundFree=true;
            }
        }
        return result;
    }

    static void runXvfbScript(String[] parameters) {
        String scriptPath = String.format("%s../../../scripts/shell/xvfb.sh",path);
        System.out.println("scriptPath: "+scriptPath);
        String result = null;
        ArrayList cmdparams = new ArrayList();
        cmdparams.add(scriptPath);
        for (String parameter : parameters) {
            cmdparams.add(parameter);
        }
        try {
            Runtime r = Runtime.getRuntime();
            Process p = r.exec((String[]) cmdparams.toArray(new String[0]));
        } catch (IOException e) {
            System.out.println(e);
        }
    }
}