package yspaceship.test;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;
import java.io.File;
import org.apache.commons.io.FileUtils;

import com.google.common.collect.ImmutableMap;

public class Chrome {
    ChromeOptions chromeOptions;
    WebDriver driver;
    JavascriptExecutor js;

    static String SCRIPT_GET_ELEMENT_BORDER;
    static String SCRIPT_UNHIGHLIGHT_ELEMENT;

    public Chrome() {
        try {
            // this runs with the head if it exists ;)
            System.setProperty("webdriver.chrome.driver", "./webdrivers/chromedriver");
            this.chromeOptions = new ChromeOptions();
            this.chromeOptions.addArguments("--verbose", "--ignore-certificate-errors");
            this.driver = new ChromeDriver();
            
            /*
            // runs headless despite physical display exist on the system;
            String displayNumber = "99";
            //String displayNumber = Utils.getFreeDisplayNumber();
            //System.out.println("Display Number: "+displayNumber);
            Utils.runXvfbScript(new String[]{"start",displayNumber});
            ChromeDriverService chromeDriverService = new ChromeDriverService.Builder()
                .usingDriverExecutable(new File("./webdrivers/chromedriver"))
                .usingAnyFreePort()
                .withEnvironment(ImmutableMap.of("DISPLAY",":"+displayNumber))
                .build();
            chromeDriverService.start();
            //ChromeOptions options = new ChromeOptions();
            //options.addArguments("--window-size=1920x1080");
            //driver = new ChromeDriver(chromeDriverService,options);
            driver = new ChromeDriver(chromeDriverService);
            
            */
            String originalPath = this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
            String getBorderScriptPath = String.format("%s../../../src/test/js/getBorder.js",originalPath);
            String releaseBorderScriptPath = String.format("%s../../../src/test/js/releaseBorder.js",originalPath);

            this.js = (JavascriptExecutor)driver;
            SCRIPT_GET_ELEMENT_BORDER = FileUtils.readFileToString(new File(getBorderScriptPath),"utf-8");
            SCRIPT_UNHIGHLIGHT_ELEMENT = FileUtils.readFileToString(new File(releaseBorderScriptPath),"utf-8"); 
        } catch (Throwable ignored) {
        }
    }


}