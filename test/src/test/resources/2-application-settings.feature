Feature: Application settings

  Users can manually input various application settings which will then be saved to configuration files. This is normally needed only for the first run of ySpaceShip. Settings used are the special purpose test database for ySpaceShip. Normally users will input credentials of their own Zotero bibliography database.

  @indoc
  Scenario Outline: Input and save settings with defaults
    Input settings of Zotero database but leave PDF editor default.

    Given "application-settings" dialogue is displayed
    When user inputs settings '<zotero_key>', '<zotero_userID>' and '<zotero_collectionID>'
    And user presses "save-application-settings" button 
    Then "application-settings" dialog closes
    And file <pbrain.conf> contains settings '<zotero_key>', '<zotero_userID>' and '<zotero_collectionID>'

  Examples: test application settings
      | zotero_key               | zotero_userID | zotero_collectionID |  
      | OdCU3l6dwtWpbOxiRRb2Naij | 3075376       | JVBN3XKB            | 

  @indoc
  Scenario Outline: Input and save all settings
    Change PDF editor for viewing files (normally needed because of different operating systems). 

    Given "application-settings" dialogue is displayed
    When user selects "local_pdf_editor" as '<local_pdf_editor>'
    And user presses "save-application-settings" button 
    Then "application-settings" dialog closes
    And file "pbrain.conf" has "local_pdf_editor" value '<local_path>'

  Examples: default settings
      | local_pdf_editor      | local_path                                                                 |
      | PDF-XChange (Windows) | C:\\\\Program Files\\\\Tracker Software\\\\PDF Viewer\\\\PDFXCview.exe     |
      | Okular (Linux)        | okular                                                                     |

  Scenario: Close dialog without saving
    Given "application-settings" dialogue is displayed
    When user presses "close-application-settings" button
    Then "application-settings" dialog closes

