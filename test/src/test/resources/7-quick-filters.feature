Feature: Quick filters
  Quick filters are a way to do quick and dirty search on the contents that is already displayed (but may be not visible due to large table) on the screen. Quick filters do not query database, are not able to search in more than one field in the table and also are not capable of complex queries. Use search filters for the above tasks.

  @indoc
  Scenario Outline: Quick filter on '<table>'
    Given the ySpaceShip is running
    When user presses '<pane-button>' button
    When user writes '<queryString>' to '<quick-filter-field>' input field
    Then '<table>' contains '<number-of-rows>' rows
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds

  Examples: quick filters
  | table | pane-button | queryString | number-of-rows | quick-filter-field |
  | publications-table | publications-list-button | Starkenburg | 1 | filter-publications |
  | sorted-annotations-table | sorted-annotations-list-button | shocks and CFs | 2 | filter-sorted-annotations |
 
