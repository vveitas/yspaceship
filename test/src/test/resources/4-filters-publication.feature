Feature: Publicatons filters and queries

  The main functionality of ySpaceShip is the ability to search and filter publications and annotations by their properties and -- most importantly -- by their full text.  A number of filter options exist in order to be able todisplay only the relevant content in the main info window.

  @indoc
  Scenario: toggle filters view
    Filters view occupies the upper part of the window and is by default open. The view can be toggled closed or open again at wish -- e.g. if more space is needed for navigating through publications and annotations.

    Given the ySpaceShip is running
    Then "library-navigation" is displayed
    When user presses "toggle-filters" (eye) icon
    Then "library-navigation" is hidden
    When user presses "toggle-filters" (eye) icon
    Then "library-navigation" is displayed

  @indoc
  Scenario: "Publication filter enabled" mode
    If publication filters are applied, user will see only a subset of all publications. Therefore it is important to know at each moment if publication filters are active. When publications filter is cleared, annotations filter is also cleared.

    Given the ySpaceShip is running
    When user writes "whatever" to "publications-full-text-query" input field
    And user presses "apply-publications-filters" button and waits for no more than "2" seconds
    Then "publications-filter-status" with "Publications filter enabled" is displayed
    Then user presses "clear-publications-filters" button and waits for no more than "2" seconds
    Then "publications-filter-status" with "Publications filter enabled" is NOT displayed
    Then "annotations-filter-status" with "Annotations filter enabled" is NOT displayed

  @indoc
  Scenario: "Annotations filter enabled" mode
    If annotations filter is applied, user will see only a subset all annotations available from the database. Therefore it is important to know at each moment if annotation filters are active. Note, that application filters can be applied hierarchically only for the publications selected by publications filter before. In that case, when annotation filters is  cleared, the publications filter still remains active as it should cleared separately.

    Given the ySpaceShip is running
    When user writes "whatever" to "annotations-full-text-query" input field
    And user presses "apply-annotations-filters" button and waits for no more than "2" seconds
    Then "annotations-filter-status" with "Annotations filter enabled" is displayed
    Then user presses "clear-annotations-filters" button and waits for no more than "2" seconds
    Then "annotations-filter-status" with "Annotations filter enabled" is NOT displayed
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds

  @indoc
  Scenario Outline: Full text query on publications' titles and file names
  	Users can search titles of the publications and names of pdf files with full text queries, regular expressions and logic constructions -- as enabled by [Elasticsearch](https://www.elastic.co/products/elasticsearch) server.

    Given the ySpaceShip is running
    When user writes '<queryString>' to "publications-full-text-query" input field
    And user presses "apply-publications-filters" button and waits for no more than "1" seconds
    Then "publications-table" contains '<number-of-rows>' rows of class "clickable-row"
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds

    Examples: query strings
    | queryString          | number-of-rows |
    | information overload | 1              |

  @indoc
  Scenario Outline: Filter by tag
    Users can filter publications by one or more tag which are listed at the top left corner of the window. Note that tags are combined with OR logical operators with other tags and filters -- so all publications which are taged with one or more of these tags are displayed.

    Given the ySpaceShip is running
    When user checks '<tag>' in "tags-list"
    And user presses "apply-publications-filters" button and waits for no more than "1" seconds
    Then "publications-table" contains '<number-of-rows>'
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds

    Examples: filtering by tag
    | tag          | number-of-rows |
    | tag1         | 3              |


  @indoc
  Scenario Outline: Filter by author
    Users can filter publications by one or more tag which are listed at the top left corner of the window. Note that tags are combined with OR logical operators with other tags and filters -- so all publications which are taged with one or more of these tags are displayed.

    Given the ySpaceShip is running
    When user checks '<author>' in "authors-list"
    And user presses "apply-publications-filters" button and waits for no more than "1" seconds
    Then "publications-table" contains '<number-of-rows>'
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds

    Examples: filtering by author
    | author          | number-of-rows |
    | Kravtsov, A. V. | 1              |

  @indoc
  Scenario Outline: Combining tags and authors in one query
    It is possible to combine all above in one publication filter. The checkbox 'combine with OR' allows to combine tags, authors and publication text either with a logical operator AND (when unchecked - default) or with OR (when checked).

    Given the ySpaceShip is running
    When user checks '<author>' in "authors-list"
    When user checks '<tag>' in "tags-list"
    When user selects '<operator>' operator
    And user presses "apply-publications-filters" button and waits for no more than "1" seconds
    Then "publications-table" contains '<number-of-rows>'
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds
 
    Examples: tag, author and title combination
    | author             | tag  | operator | number-of-rows |  
    | Heylighen, Francis | tag1 | AND      | 1              |  
    | Heylighen, Francis | tag1 | OR       | 3              |  


  @indoc
  Scenario Outline: Combining tags and full text search on publication title in one filter
    It is possible to combine all above in one publication filter. The checkbox 'combine with OR' allows to combine tags, authors and publication text either with a logical operator AND (when unchecked - default) or with OR (when checked).

    Given the ySpaceShip is running
    When user checks '<tag>' in "tags-list"
    When user writes '<queryString>' to "publications-full-text-query" input field
    When user selects '<operator>' operator
    And user presses "apply-publications-filters" button and waits for no more than "1" seconds
    Then "publications-table" contains '<number-of-rows>'
    Then user presses "clear-publications-filters" button and waits for no more than "1" seconds
 
    Examples: tag and publication title combination
    | tag  | queryString                      | operator | number-of-rows |  
    | tag2 | Gauss-Bonnet Type Identity | AND      | 1              |  
    | tag2 | Gauss-Bonnet Type Identity | OR       | 2              |  


 @indoc
  Scenario: Helper feature: Toggle selecting all publications

  By default all publications are not checked in the publication list. If a user wants to search all publications returned after, say search for one author, they need to be selected by the help of the checkbox near each title. All of them can be selected at once by checking the one near 'Title' column heading.

    Given the ySpaceShip is running
    When all items in "publications-table" are unchecked
    When user toggles "toggle-all-head"
    Then all items in "publications-table" are checked
 
