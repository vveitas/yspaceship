Feature: Control room

  Users should be able to set and save application settings needed for 
  it to run;

  @indoc
  Scenario: Clear SpaceGraph Database
    Given "control-room" dialogue is displayed
    When user presses "clearSpaceGraph" button and waits for script to finish
    Then "clearSpaceGraph" button text is "SpaceGraph is empty.."
    And the spacegraph is cleared of all contents    

  @indoc @ignore
  Scenario: Synchronize graph with no settings - unsucessfull
    Given "control-room" dialogue is displayed
    Given file <pbrain.conf> does not contain valid settings
    When user presses "synchronizeSpaceGraph" button
    Then "invalid-settings" dialog is displayed

	Scenario Outline: initiate individual synchronization steps
    Given "control-room" dialogue is displayed
    Given file <pbrain.conf> contains valid settings:
    | zotero_key          | OdCU3l6dwtWpbOxiRRb2Naij |  
    | zotero_userID       | 3075376                  |  
    | zotero_collectionID | JVBN3XKB                 |  
    When user presses '<runSingleStepbutton>' button for starting '<step-name>'
    Then element '<indicator>' has attribute "class": "success"
    Then element '<percentage-info>' has text: "100%"
    Then element '<progress-info>' has attribute "value": "100"

    Examples: synchronization step indicator paths
      | step-name              | runSingleStepbutton                             | indicator                                | percentage-info                               | progress-info                                     |  
      | zotero-synchronization | //*[@id="zotero-synchronization"]/div[1]/button | //*[@id="zotero-synchronization"]/div[3] | //*[@id="zotero-synchronization"]/div[4]/span | //*[@id="zotero-synchronization"]/div[5]/progress |  
      | getting-pdf-library    | //*[@id="getting-pdf-library"]/div[1]/button    | //*[@id="getting-pdf-library"]/div[3]    | //*[@id="getting-pdf-library"]/div[4]/span    | //*[@id="getting-pdf-library"]/div[5]/progress    |  
      | extracting-annotations | //*[@id="extracting-annotations"]/div[1]/button | //*[@id="extracting-annotations"]/div[3] | //*[@id="extracting-annotations"]/div[4]/span | //*[@id="extracting-annotations"]/div[5]/progress |  
      | loading-spacegraph     | //*[@id="loading-spacegraph"]/div[1]/button     | //*[@id="loading-spacegraph"]/div[3]     | //*[@id="loading-spacegraph"]/div[4]/span     | //*[@id="loading-spacegraph"]/div[5]/progress     |  
  
  @indoc
  Scenario: Synchronize graph
    Given "control-room" dialogue is displayed
    Given file <pbrain.conf> contains valid settings:
    | zotero_key          | OdCU3l6dwtWpbOxiRRb2Naij |  
    | zotero_userID       | 3075376                  |  
    | zotero_collectionID | JVBN3XKB                 |  
    When user presses "synchronizeSpaceGraph" button and waits for script to finish
    Then "synchronizeSpaceGraph" button text is "Synchronized"
    And user presses "close-and-refresh" button and waits for script to finish