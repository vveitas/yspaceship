Feature: Specific scenarios

This feature documents specific scenarios of ySpaceShip usage.

@indoc
Scenario Outline: using hierarchical filters sequentially
	Often user wants to find annotations with specific keywords or phrases only in certain publications or publications by specific author. The way to achieve this in ySpaceShip is to use 

	Given the ySpaceShip is running
	When user checks '<author>' in "authors-list"
	And user presses "apply-publications-filters" button and waits for no more than "1" seconds
	When user checks '<publication>' in "publications-table"
	When user writes '<queryString>' to "annotations-full-text-query" input field
	And user presses "apply-annotations-filters" button and waits for no more than "2" seconds
	Then "sorted-annotations-table" contains '<number-of-rows>' rows of class "clickable-row"
	Then user presses "clear-annotations-filters" button and waits for no more than "1" seconds

	Examples: hierarchical search
	| author           | publication                                     | queryString | number-of-rows |  
	| Babourova, O. V. | Gauss-Bonnet Type Identity in Weyl-Cartan Space | comment     | 2              |  


@indoc @ignore
Scenario: opening PDF file at the page of annotation
	User can open PDF files directly from ySpaceShip interface at the page of chosen annotation.

	Given the ySpaceShip is running
	When user presses "sorted-annotations-table" button
	And user selects row with "//*[@uniquename="bd30e4b9-bf12-4ebf-8143602a9bdd7877"]"
	And user presses "copy-annotation-command" button
	And user opens command window in operating system
	And user pastes the copied command
	Then PDF file at corresponding page is opened

