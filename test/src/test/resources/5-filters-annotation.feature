Feature: Annotation filters

	@indoc
	Scenario Outline: Get all annotations of selected publication
	  Sometimes it is needed to get all annotations of particular publication and simply browse them. If the database is small (i.e. all annotations fit into the 'browser memory'), this can be done simply by going to 'sorted annotations' panel and finding the required publication there. Yet most of the time this is not possible and an annotations filter by publication has to be applied first.

	  Given the ySpaceShip is running
	  When user checks '<publication>' in "publications-table"
	  And user presses "apply-annotations-filters" button and waits for no more than "1" seconds
	  Then "sorted-annotations-table" contains '<number-of-sorted-annotation-rows>'
	  Then user presses "clear-annotations-filters" button and waits for no more than "1" seconds

	  Examples: filtering by publication
	  | publication                                             | number-of-annotation-rows | number-of-sorted-annotation-rows |  
	  | A Computation in a Cellular Automaton Collider Rule 110 | 1                         | 2                                |  

	@indoc
	Scenario Outline: Full text query on highlights and comments of all publications
		Users can search highlights and comments together (default) or individually (by unchecking appropriate check-box below query field) of all publications in the database.

	  Given the ySpaceShip is running
	  When user writes '<queryString>' to "annotations-full-text-query" input field
	  And user presses "apply-annotations-filters" button and waits for no more than "2" seconds
	  Then "sorted-annotations-table" contains '<number-of-rows>' rows of class "clickable-row"
	  Then user presses "clear-annotations-filters" button and waits for no more than "1" seconds

	  Examples: query strings
	  | queryString                    | number-of-rows |  
	  | information overload           | 2              |  
	  | information OR overload        | 2              |  
	  | "information overload"         | 1              |  
	  | information AND  overload      | 1              |  
	  | ^.*\binformation overload\b.*$ | 1              |  

	@indoc
	Scenario Outline: Full text query on highlights and comments of selected publications
		Users can search highlights and comments together (default) or individually (by unchecking appropriate check-box below query field) only for the selected publications in the publication pane.

	  Given the ySpaceShip is running
	  When user checks '<publication>' in "publications-table"
	  When user writes '<queryString>' to "annotations-full-text-query" input field
	  And user presses "apply-annotations-filters" button and waits for no more than "2" seconds
	  Then "sorted-annotations-table" contains '<number-of-rows>' rows of class "clickable-row"
	  Then user presses "clear-annotations-filters" button and waits for no more than "1" seconds

	  Examples: query strings
	  | publication                                                                                           | queryString          | number-of-rows |  
	  | Complexity and Information Overload in Society: why increasing efficiency leads to decreasing control | information overload | 1              |  
