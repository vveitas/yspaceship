Feature: Application management

The web interface is a window to the SpaceGraph -- the database where all publications and their annotations are stored. Application management feature allows to control what is loaded into space graph and some other options.

@indoc
Scenario: open application management menu
	The management menu is acceptable through the gear icon at top right of the window. It opens a menu to a few dialogs -- the exact number may vary as more management functions are added.

	Given the ySpaceShip is running
	When user presses gear icon
	Then application management menu is opened

@indoc
Scenario: open application settings dialog
	Application settings contain Zotero database configuration and pdf browser configuration

	Given the "management" menu is open
	When user chooses "application-settings-menu-item"
	Then "settings-form" is displayed

@indoc
Scenario: open control room dialog
	Control room dialogue allows to synchronize SpaceGraph with Zotero database and extract pdf annotations from PDF files.

	Given the "management" menu is open
	When user chooses "control-room-menu-item"
	Then "control-room" is displayed

@indoc
Scenario: display help
	User manual / help system is accessble via question-mark icon and opens as a separate web page.

	Given the ySpaceShip is running
	When user presses "help" icon
	Then user guide opens at page "Features · GitBook" in separate browser tab
