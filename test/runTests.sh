#!/bin/bash

#sh scripts/shell/xvfb.sh start
#export DISPLAY=:1.5
gradle wrapper
./gradlew test
#scripts/shell/xvfb.sh stop
scripts/shell/build-test-report.sh
cp target/cucumber-json-report.json ../doc/_temp/
