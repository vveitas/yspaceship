# System initialization

	|Parameter           |Value                    |
	|--------------------|-------------------------|
	|zotero_key          |OdCU3l6dwtWpbOxiRRb2Naij |
	|zotero_userID       |3075376                  |
	|zotero_collectionID |JVBN3XKB  	           |

## Check if config file is complete

* <Parameter> is not empty in the pbrain.conf file

## Saving the credentials required for Zotero

Tags: skip

* User provides credentials to connect to Zotero database
     
* Credentials are saved for later reuse so that user does not need to enter them again

## Saving the credentials required for PDF library database

Tags: skip

* User provides credentials to access PDF files

* Credentials are saved for later reuse so that user does not need to enter them again


## Checking the connection to Zotero

* Connect to Zotero library with provided credentials
* The connection is successful

## Database integrity check

* User is logged in to the system
* User presses a button 'check database integrity'
* Database integrity is checked
