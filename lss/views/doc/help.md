# Short manual and some help on the way...

## Showing files in PDF browser

Files are accessed via the local web server serving the PDF library directory, specified on startup. On most browsers, PDF files are rendered using [Mozilla's pdf.js](https://mozilla.github.io/pdf.js/) library. This library by default does not render PDF annotations. In order to see annotations, a file has to be downloaded and opened locally.
Or, an alternative/unofficial [distribution of pdf.js library]() can be installed for [Mozilla Firefox](http://mozilla.github.io/pdf.js/extensions/firefox/pdf.js.xpi) or [Google Chrome](https://chrome.google.com/webstore/detail/pdf-viewer/oemmndcbldboiebfnladdacbdfmadadm) which renders annotations in the browser by default.
