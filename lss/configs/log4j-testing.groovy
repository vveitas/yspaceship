log4j {
    rootLogger="WARN, PBrainFileAppender"
    logger."net.vveitas.pbrainmodular"="WARN, PBrainFileAppender"
    additivity."net.vveitas.pbrainmodular"=false
//    appender.PBrainFileAppender = "org.apache.log4j.FileAppender"
//    appender."PBrainFileAppender.File"="logs/testing.log"
    appender."PBrainFileAppender.Append"=true

    appender.PBrainFileAppender="org.apache.log4j.DailyRollingFileAppender"
    appender."PBrainFileAppender.File"="logs/rolling.log"
    appender."PBrainFileAppender.DatePattern"="'.'yyyy-MM-dd"
    appender."PBrainFileAppender.Append"=true

    appender."PBrainFileAppender.layout"="org.apache.log4j.PatternLayout"
    appender."PBrainFileAppender.layout.ConversionPattern"= "%-4r %d [%t] %-5p %c: %m%n"
}
