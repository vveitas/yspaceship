module.exports = {
// order is important !!!
    synchronizationStepArray: [
        {
          name: 'zotero-synchronization',
          description: 'Scanning remote Zotero database',
          function: 'updatePublications',
          report: 'Retrieved all publications from Zotero database'
        },
        {
          name: 'getting-pdf-library',
          description: 'Scanning local PDF library',
          function: 'getPDFLibrary',
          report: 'Retrieved all files from PDF library.'
        },
        {
          name: 'extracting-annotations',
          description: 'Extracting PDF annotations',
          function: 'extractAnnotations',
          report: 'Extracted annotations from PDF files.'
        },
        { name: 'loading-spacegraph',
          description: 'Loading SpaceGraph data',
          function: 'getNonFilteredResults',
          report: 'Got all all data from SpaceGraph'
        }
    ],
    publicationsFullTextSearchFields: {
      publicationTitle: "Title of the publication",
      title: "Title of the journal or book",
    },
    annotationsFullTextSearchFields: {
      popupContents: "Text of annotation / comment",
      highlightedText: "Highlighted text",
      uniqueName: "annotation uniqueName"
    },
    fullTextSearchFields: {
        authorstext: "Authors in publication",
        editorstext: "Editors in publication",
        publicationTitle: "Title of the publication",
        title: "Title of the journal or book",
        popupContents: "Text of annotation / comment",
        highlightedText: "Highlighted text"
    }

}; // end exports
