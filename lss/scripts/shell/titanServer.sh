#!/bin/sh

if [ $1 == 'start' ]; then
    ../required/titan-server-0.4.4/bin/titan.sh -c cassandra start
else
    if [ $1 == 'stop' ]; then
        ../required/titan-server-0.4.4/bin/titan.sh stop
    else
        if [ $1 == 'status' ]; then
            ../required/titan-server-0.4.4/bin/titan.sh status
        fi
    fi
fi
