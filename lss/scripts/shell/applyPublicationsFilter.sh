#!/bin/sh

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH
export PATH=$JAVA_HOME/bin:$PATH
export TITAN_HOME=/opt/yspaceship/required/titan-server-0.4.4

ARGS="$1"
# run groovy script / uncompiled:
#groovy -cp scripts/groovy scripts/groovy/enableFilters.groovy $ARGS
# run compiled java class
#java -cp "scripts/java:/$GROOVY_HOME/lib/*:/$TITAN_HOME/lib/*" EnableFilters $ARGS
# pass socket message to running server (temporary as this can be done via nodejs directly...):
#echo "{\"class\":\"applyPublicationsFilter\",\"data\":$ARGS}" | netcat 127.0.0.1 3003
echo "{\"class\":\"ApplyPublicationsFilter\",\"data\":$ARGS}" | netcat 127.0.0.1 3003

# for profiling with visualvm
#java -agentpath:/opt/visualvm_139/profiler/lib/deployed/jdk16/linux-amd64/libprofilerinterface.so=/opt/visualvm_139/profiler/lib,5141 -cp "scripts/java:/$GROOVY_HOME/lib/*:/$TITAN_HOME/lib/*" EnableFilters $ARGS
exit 0
