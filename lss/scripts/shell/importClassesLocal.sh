#!/bin/sh

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH
export PATH=$JAVA_HOME/bin:$PATH
export TITAN_HOME=/opt/yspaceship/required/titan-server-0.4.4

groovy -cp scripts/groovy scripts/groovy/ImportClassesLocal.groovy

cp -r ~/.groovy/grapes /opt/yspaceship/required/groovy/

exit 0
