#!/bin/sh

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH
export PATH=$JAVA_HOME/bin:$PATH
export TITAN_HOME=/opt/yspaceship/required/titan-server-0.4.4

if [ "$1" == "start" ]; then
  sh ./scripts/shell/elasticsearch.sh start;
  sh ./scripts/shell/titanServer.sh start;
#  groovy -cp scripts/groovy scripts/groovy/ySpaceShip.groovy &
  java -cp "scripts/java:/$GROOVY_HOME/lib/*:/$TITAN_HOME/lib/*" ySpaceShip &
#  java -cp "scripts/java:/$GROOVY_HOME/lib/*:/$TITAN_HOME/lib/*" WarmCaches;
  ./node_modules/http-server/bin/http-server -p 3002 /opt/yspaceship/pdflibrary &
  npm start;
fi
if [ "$1" == "stop" ]; then
  sh ./scripts/shell/titanServer.sh stop;
  sh ./scripts/shell/elasticsearch.sh stop;
  JAVA_PID=$(ps aux | grep ySpaceShip | grep -v grep | awk '{print $2}')
  kill $JAVA_PID
  kill &(ps aux | grep npm);
  httppid=$(ps aux | grep "http-server -p 3002" | grep -v grep | awk '{print $2}');
  kill $httppid;
fi
