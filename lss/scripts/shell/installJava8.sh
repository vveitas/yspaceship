#!/bin/sh

installDir="$1"
sudo -s
cd $installDir
if [ ! -d jdk1.8.0_74 ]; then
  if [ ! -f jdk-8u74-linux-x64.tar.gz ]; then
    wget http://download.oracle.com/otn-pub/java/jdk/8u74-b02/jdk-8u74-linux-x64.tar.gz
  fi
  tar xf jdk-8u74-linux-x64.tar.gz
else
  echo "Java8 already exists in /usr/lib/jvm/ - not installing"
fi
