    	use BibTeX::Parser;
        use IO::File;
	use YAML;

    my $fh     = IO::File->new("library.bib");

    # Create parser object ...
    my $parser = BibTeX::Parser->new($fh);
    
    # ... and iterate over entries
    while (my $bibentry = $parser->next ) {
            if ($bibentry->parse_ok) {
		my $entry = {
			key => $bibentry->key,
			type => $bibentry->type,
			title => $bibentry->field("title"),
			authors => $bibentry->field("author"),
			editors => $bibentry->editor,
		};
		print Dump($entry);
		print "\n";
            } else {
                    warn "Error parsing file: " . $entry->error;
            }
    }
