import os
import sys

import popplerqt4
import PyQt4

walk_dir = sys.argv[1]

file_count = 0
annotated_file_count = 0

for root, subdirs, files in os.walk(walk_dir):
    for fullname in files:
        # walk the directory path, collect all pdf files and analyse them:
        if fullname.endswith('.pdf'): 
            filepath = os.path.join(root, fullname)
            doc = popplerqt4.Poppler.Document.load(filepath)
            annotation_count = 0
            file_count += 1
            for i in range(doc.numPages()):
                page = doc.page(i)
                annotations = page.annotations()
                annotation_count += len(annotations)
            if annotation_count is not 0:
                annotated_file_count +=1
                relpath = os.path.relpath(filepath, "/media/data/weaver-library/Phd items 10-8-2014/")
                print('%s) File: %s; Annotation count: %s ' % (annotated_file_count, relpath, annotation_count))

print('\n-- Total file count %s; \n-- Annotated file count %s ' %(file_count,annotated_file_count))
    