import popplerqt4
import sys
import PyQt4
import ftfy
import hashlib
import ntpath
import json
# for poppler documentation, check this: https://people.freedesktop.org/~aacid/docs/qt4/classPoppler_1_1Annotation.html


def main():

    doc = popplerqt4.Poppler.Document.load(sys.argv[1])

    # fileNameHash probably not needed, but saving this anyway - could be useful in the future...
    h = hashlib.md5()
    h.update(ntpath.basename(sys.argv[1]))
    fileNameHash = h.hexdigest()

    total_annotations = 0
    documentAnnotations = {}
    for i in range(doc.numPages()):
        annotationData = {}
        page = doc.page(i)
        annotations = page.annotations()
        (pwidth, pheight) = (page.pageSize().width(), page.pageSize().height())
        if len(annotations) > 0:
            for annotation in annotations:
                if  isinstance(annotation, popplerqt4.Poppler.Annotation):
                    total_annotations += 1
                    if(isinstance(annotation, popplerqt4.Poppler.HighlightAnnotation)):
                        annotationData = {}
                        annotationData['type'] = 'highlight'
                        annotationData['subType'] = getHighlightName(annotation.highlightType())
                        annotationData['author'] = process(annotation.author())
                        annotationData['creationDate'] = process(annotation.creationDate().toTime_t())
                        annotationData['modificationDate'] = process(annotation.modificationDate().toTime_t())
                        annotationData['page'] = i
                        if hasattr(annotation, 'style'):
                            annotationData['color'] = annotation.style().color()

                        quads = annotation.highlightQuads()
                        txt = ""
                        for quad in quads:
                            rect = (quad.points[0].x() * pwidth,
                                    quad.points[0].y() * pheight,
                                    quad.points[2].x() * pwidth,
                                    quad.points[2].y() * pheight)
                            bdy = PyQt4.QtCore.QRectF()
                            bdy.setCoords(*rect)
                            txt = txt + unicode(page.text(bdy)) + ' '

                        rectangle = {}
                        boundary = annotation.boundary()

                        rectangle['topLeftX'] = process(boundary.topLeft().x())
                        rectangle['topLeftY'] = process(boundary.topLeft().y())
                        rectangle['topRightX'] = process(boundary.topRight().x())
                        rectangle['topRightY'] = process(boundary.topRight().y())
                        rectangle['bottomLeftX'] = process(boundary.bottomLeft().x())
                        rectangle['bottomLeftY'] = process(boundary.bottomLeft().y())
                        rectangle['bottomRightX'] = process(boundary.bottomRight().x())
                        rectangle['bottomRightY'] = process(boundary.bottomRight().y())
                        rectangle['width'] = process(boundary.width())
                        rectangle['height'] = process(boundary.height())
                        annotationData['rectangle'] = rectangle

                        annotationData['highlightedText'] = process(txt)
                        annotationData['popupContents'] = process(annotation.contents())
                        uniqueName = process(annotation.uniqueName())
                        if (uniqueName == ""):
                            h.update(ntpath.basename(annotationData.get('author','')+annotationData.get('highlightedText','')+annotationData.get('popupContents','')));
                            uniqueNameHash = h.hexdigest()
                            uniqueName = 'python-uniqueName-'+uniqueNameHash
                        annotationData['uniqueName'] = uniqueName
                        documentAnnotations[uniqueName] = annotationData

                    if(isinstance(annotation, popplerqt4.Poppler.TextAnnotation)):
                        annotationData = {}
                        annotationData['type'] = 'text'
                        annotationData['subType'] = 'text'
                        annotationData['author'] = process(annotation.author())
                        annotationData['creationDate'] = process(annotation.creationDate().toTime_t())
                        annotationData['modificationDate'] = process(annotation.modificationDate().toTime_t())
                        annotationData['page'] = i
                        rectangle = {}
                        boundary = annotation.boundary()

                        rectangle['topLeftX'] = process(boundary.topLeft().x())
                        rectangle['topLeftY'] = process(boundary.topLeft().y())
                        rectangle['topRightX'] = process(boundary.topRight().x())
                        rectangle['topRightY'] = process(boundary.topRight().y())
                        rectangle['bottomLeftX'] = process(boundary.bottomLeft().x())
                        rectangle['bottomLeftY'] = process(boundary.bottomLeft().y())
                        rectangle['bottomRightX'] = process(boundary.bottomRight().x())
                        rectangle['bottomRightY'] = process(boundary.bottomRight().y())
                        rectangle['width'] = process(boundary.width())
                        rectangle['height'] = process(boundary.height())
                        annotationData['rectangle'] = rectangle

                        annotationData['popupContents'] = process(annotation.contents())

                        uniqueName = process(annotation.uniqueName())
                        if (uniqueName == ""):
                            h.update(ntpath.basename(annotationData.get('author','')+annotationData.get('highlightedText','')+annotationData.get('popupContents','')));
                            uniqueNameHash = h.hexdigest()
                            uniqueName = 'python-uniqueName-'+uniqueNameHash

                        documentAnnotations[uniqueName] = annotationData


    json_data = json.dumps(documentAnnotations)
    print json_data

def process(obj):
    return unicode(obj).encode('utf8','replace').replace("\n","")

def getHighlightName(argument):
    switcher = {
        0: "highlight",
        1: "squiggly",
        2: "underline",
        3: "strikeout"
    }
    return switcher.get(argument, "nothing")

def dump(obj):
    return_string = ""
    for attr in dir(obj):
       if hasattr( obj, attr ):
           string =  "obj.%s = %s" % (attr, getattr(obj, attr))
           return_string = return_string + string + "\n"
    return return_string

if __name__ == "__main__":
    main()
