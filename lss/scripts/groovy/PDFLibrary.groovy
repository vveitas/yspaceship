@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked

import ySpaceShip

import groovy.io.FileType
import org.apache.commons.io.FilenameUtils
import com.tinkerpop.pipes.util.structures.Table

import com.tinkerpop.blueprints.Vertex
import groovy.json.JsonSlurper

import com.thinkaurelius.titan.core.TitanGraph

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.log4j.PropertyConfigurator
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin

import org.apache.commons.io.FilenameUtils

class PDFLibrary {

  static TitanGraph g = ySpaceShip.g      
  static Logger logger
  static Map config = new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/configs/pbrain.conf"))
  static int createdNewFileVertices = 0
  static int createdPublicatonToFileEdges = 0
  static Map attachedFilesMap

  @TypeChecked
  static main(args) {

    // setting up logging stuff
    def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
    PropertyConfigurator.configure(config.toProperties())
    logger = LoggerFactory.getLogger('PDFLibrary.class');

    List listOfFiles = getListOfFiles()
    attachedFilesMap = getAttachedFilesMap()
    def processedFiles = 0;
    def numberOfFilesInLibrary = listOfFiles.size();
    ySpaceShip.sg.emitProgress('getting-pdf-library', (double) processedFiles/numberOfFilesInLibrary);

    listOfFiles.each { String filePath ->
      createFileVertexInSpaceGraph(filePath)
      processedFiles +=1;
      ySpaceShip.sg.emitProgress('getting-pdf-library',(double) processedFiles/numberOfFilesInLibrary);
    }

    logger.warn("Created {} new file vertexes and {} publication to file edges in ySpaceShip.", createdNewFileVertices, createdPublicatonToFileEdges)
    //print "{createdNewFileVertices:"+createdNewFileVertices+", createdPublicatonToFileEdges:"+createdPublicatonToFileEdges +"}"
  }

  @TypeChecked
  static List<String> getListOfFiles() {
      logger.warn("Getting the list of all files in the Library")

      List<String> listOfFiles = []

      def dir = new File((String) config.pdf_library_dir)
      dir.eachFileRecurse (FileType.FILES) { file ->
          def filePath = file.getAbsolutePath().minus(config.pdf_library_dir)
          def fileType = new FilenameUtils().getExtension(filePath);
          if (fileType == "pdf") {
            listOfFiles << filePath
            logger.warn("Got filePath {}", filePath)
          } else {
            logger.warn("File {} is not PDF - skipping",filePath);
          }
      }

      logger.warn("Retrieved {} files from PDF library", listOfFiles.size())
      return listOfFiles
  }

  @TypeChecked
  static Map getAttachedFilesMap() {
      Map m = [:]
      g.query().has('entry','publication').hasNot("file","NoAttachment").vertices().each { 
        def key = checksum(it.getProperty("file").toString());
        def value = it.getProperty("key").toString(); 
        m[key] = value;
      };
      logger.warn("Retrieved zotero keys mapped to file names {} from SpaceGraph", m)
      return m
  }

  /**
  * creates a new file vertex if does not exist and returns it;
  * if the vertex does exist, returns null
  **/
  @TypeChecked
  static Vertex createFileVertexInSpaceGraph(String filePath) {
      def fileName = new FilenameUtils().getName(filePath)
      def fileNameHash = checksum(fileName).toString()
      Vertex fileVertex
      if ( ! g.query().has("fileNameHash",fileNameHash).vertices().iterator().hasNext() ) {
          logger.warn("Calculated fileNameHash {} for fileName {}", fileNameHash, fileName)
          fileVertex = g.addVertex(null)
          fileVertex.setProperty("fileName",fileName);
          fileVertex.setProperty("entry","PDFfile");
          fileVertex.setProperty("fileNameHash",fileNameHash);
          fileVertex.setProperty("relativeFilePath",filePath);
          fileVertex.setProperty("fileModified",0);
          g.commit()
          createdNewFileVertices += 1
          logger.warn("Created a new fileVertex {} for file {} in ySpaceShip.", fileVertex.toString(), fileName)
          String zotero_key = attachedFilesMap.get(fileNameHash)
          logger.warn("Checking attachedFilesMap: ")
          attachedFilesMap.each {key, value ->
              logger.warn("Key: {}; value: {}", key, value)
              if (key == fileNameHash) {
                  logger.warn("fileNameHash {} and key {} are equivalant", fileNameHash, key)
              } else {
                  logger.warn("fileNameHash {} and key {} are NOT equivalant", fileNameHash, key)
              }
          }
          logger.warn("zotero_key of publicationVertex with fileNameHash {} (i.e. fileName {} )is {}", fileNameHash, fileName, zotero_key)
          if (zotero_key != null ) {
              Vertex publicationVertex = getPublicationVertex(zotero_key)
              g.addEdge(null,publicationVertex,fileVertex,'pdfAttachment')
              g.commit()
              logger.warn("Added link from publicationVertex {} to fileVertex {}.", publicationVertex.toString(), fileVertex.toString())
              logger.warn("i.e. publication titled {} is linked to file named {}.", publicationVertex.getProperty("title"), fileVertex.getProperty("file"))
              createdPublicatonToFileEdges += 1
          } else {
              logger.warn("publicationVertex for fileNameHash {} (i.e. fileName {}) does not exist in SpaceGraph (also probably in Zotero)", fileNameHash, fileName)
          }
      } else {
          logger.warn("File vertex for file {} already exists -- returning null.", fileName)
      }
      return fileVertex
  }

  @TypeChecked
  static Vertex getPublicationVertex(String zotero_key) {
      Vertex publicationVertex = g.query().has("key",zotero_key).vertices().iterator().next()
      logger.warn("Retrieved publicationVertex {} with zotero_key {}", publicationVertex.toString(), zotero_key)
      return publicationVertex
  }

  @TypeChecked
  static String checksum( String input ) {
      def digest = java.security.MessageDigest.getInstance("MD5")
      digest.update( input.bytes )
      def checksum = new BigInteger(1,digest.digest()).toString(16).padLeft(32, '0')
      logger.warn("Calculated MD checksum {} for string {}", checksum, input)
      return checksum
  }

}
