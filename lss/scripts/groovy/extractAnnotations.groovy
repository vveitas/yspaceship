@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked

import ySpaceShip
 
import org.slf4j.Logger
import org.apache.log4j.PropertyConfigurator
import org.slf4j.LoggerFactory
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration

import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge

import groovy.json.JsonSlurper
import java.io.InputStream

import java.lang.Long

import org.apache.commons.io.FilenameUtils
import java.util.concurrent.TimeUnit

import static org.junit.Assert.assertNotNull

import com.tinkerpop.gremlin.java.GremlinPipeline
import com.tinkerpop.pipes.PipeFunction

class ExtractPDFAnnotations {

  static TitanGraph g = ySpaceShip.g
  static Logger logger
  static Map config = new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/configs/pbrain.conf"))

  @TypeChecked
  static main(args) {

    // setting up logging stuff
    def logConfig = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
    PropertyConfigurator.configure(logConfig.toProperties())
    logger = LoggerFactory.getLogger('ExtractPDFAnnotations.class');

    // get the list of files in the database:
    List<Vertex> fileVertexListForModifiedFiles = getFileVertexListFromSpaceGraph();
    def processedFiles = 0;

    def numberOfFilesToModify = fileVertexListForModifiedFiles.size() ?: processedFiles;
    if (numberOfFilesToModify !=0) {
      ySpaceShip.sg.emitProgress('extracting-annotations',(double) processedFiles/numberOfFilesToModify);
    } else {
      ySpaceShip.sg.emitProgress('extracting-annotations', 1.0d);
    }
    logger.warn("Got file map for modification: {}", fileVertexListForModifiedFiles)
    fileVertexListForModifiedFiles.each { Vertex fileVertex ->
        def fullFilePath = config.pdf_library_dir.toString() + fileVertex.getProperty("relativeFilePath")
        def annotations = getPDFAnnotations(fileVertex,fullFilePath)
        updateFileAnnotationsInSpaceGraph(fileVertex, annotations)
        connectRelatedAnnotations(fileVertex)
        def modifiedDateOnDisk = new Date(new File(fullFilePath).lastModified())
        logger.warn("File {} modified on disk {} ", fileVertex.getProperty("relativeFilePath"), modifiedDateOnDisk)
        fileVertex.setProperty("modifiedDate",modifiedDateOnDisk)
        g.commit()
        processedFiles +=1;
        ySpaceShip.sg.emitProgress('extracting-annotations',(double) processedFiles/numberOfFilesToModify);
        logger.warn("Updated fileVertex {} modifiedDate to {}", fileVertex.getProperty("fileName"), modifiedDateOnDisk)
    }
  }

  @TypeChecked
  static List getFileVertexListFromSpaceGraph() {
    List<Vertex> fileVertexList = g.query().has("entry","PDFfile").vertices().toList();
    logger.warn('Retrieved {} fileVertexes from SpaceGraph', fileVertexList.size());
    logger.warn('Contructing list fileVertexes which are not synchronized with SpaceGraph')
    def fileVertexListForModifiedFiles = []
    fileVertexList.each { fileVertex ->
        def fullFilePath = config.pdf_library_dir.toString() + fileVertex.getProperty("relativeFilePath")
        def file = new File(fullFilePath)
        def fileModifiedOnDisk = new Date(file.lastModified())
        logger.warn("File {} modified on disk {} ", fileVertex.getProperty("relativeFilePath"), fileModifiedOnDisk)
        Date fileModifiedOnSpaceGraph = (Date) fileVertex.getProperty("modifiedDate")
        logger.warn("File {} modified on SpaceGraph {} ", fileVertex.getProperty("relativeFilePath"), fileModifiedOnSpaceGraph)
        if (fileModifiedOnDisk > fileModifiedOnSpaceGraph) {
          logger.warn("fileModifiedOnDisk > fileModifiedOnSpaceGraph, therefore putting for modification {}.", fileVertex.getProperty("fileName"))
          fileVertexListForModifiedFiles.add(fileVertex)
        }
    logger.warn("fileVertexListForModifiedFiles is {}",fileVertexListForModifiedFiles)
    }
    return fileVertexListForModifiedFiles;
  }

  static Object getPDFAnnotations(Vertex fileVertex, String fullFilePath) throws Throwable {
      def fileType = new FilenameUtils().getExtension(fullFilePath);
      logger.warn("Attached file type is {}", fileType);
      def json;
      InputStream inputStream;
      def timeoutEvent;

      if (fileType != "pdf") {
        logger.warn("Only PDF files are being scanned so far, skipping and recording zero annotations");
      } else {
        try {
          logger.warn("Extracting annotations from file {}",fullFilePath);
          ProcessBuilder pb = new ProcessBuilder("python", "./scripts/python/extractDocumentAnnotations.py", fullFilePath, "2>/dev/null");
          Process p = pb.start();
          logger.warn("External process {} started", p)
          timeoutEvent = new Timer("KillProcess");
          timeoutEvent.schedule(new KillProcess(p),60*1000L);
          inputStream = p.getInputStream();
          json = new JsonSlurper().parse(inputStream);
          logger.warn("Generated json data object {} ",json)
        } catch (groovy.json.JsonException e) {
          logger.warn("Exception thrown: {}", e.getMessage());
          json = new JsonSlurper().parseText("{}");
          logger.warn("Returning empty json object: {}", json)
          fileVertex.setProperty("annotationExtractionError","extractDocumentAnnotations script failed");
          logger.warn("Flagging fileVertex {} with property annotationExtractionError: {}", fileVertex.toString(), fileVertex.getProperty("annotationExtractionError"));
          g.commit();
        } finally {
          if (inputStream != null) {
              inputStream.close();
              logger.warn("Closing input stream");
              timeoutEvent.cancel();
              logger.warn("Cancelling timeout event - process finished within scheduled time")
          }
        }
      }
      return json;

  }

  @TypeChecked
  static getPDFAnnotationsOld(String fullFilePath) {
      def fileType = new FilenameUtils().getExtension(fullFilePath);
      logger.warn("Attached file type is {}", fileType);
      def fileAnnotations;
      if (fileType !="pdf") {
        logger.warn("Only PDF files are being scanned so far, skipping and recording zero annoations");
      } else {
        logger.warn("Extracting annotations from file {}",fullFilePath);
        ProcessBuilder pb = new ProcessBuilder("python", "./scripts/python/extractDocumentAnnotations.py", fullFilePath, "2>/dev/null");
        logger.warn("Executing python script {}", pb)
        Process p = pb.start();
        def response = "{}";
        if(!p.waitFor(5, TimeUnit.MINUTES)) {
            //timeout - kill the process.
            p.destroy(); // consider using destroyForcibly instead
            logger.warn("The process does not return after 5 minutes - killing and returning empty annotation map")
        } else {
            response = p.in.text
        }
        assertNotNull(response)
        logger.warn("Received response {} ",response)
        fileAnnotations =  (List) new JsonSlurper().parseText(response)
        logger.warn("Retrieved fileAnnotations json object of size {}", fileAnnotations.size())
        logger.warn("Retrieved fileAnnotations json object: {}", fileAnnotations)
      }
      return fileAnnotations
  }

  @TypeChecked
  static updateFileAnnotationsInSpaceGraph(Vertex fileVertex, Object annotations) {
    logger.warn("Updating annotations for fileVertex {}, i.e. file {}", fileVertex.toString(), fileVertex.getProperty("fileName"))
    annotations.each {String annotationKey, Map extractedAnnotationProperties ->
        if (annotationKey == "" | annotationKey == null) {
            logger.warn("AnnotationKey of the publication is empty. This may happen if the annotation was created on mobile device.");
            String popupContentsTemp = extractedAnnotationProperties.popupContents ?: '';
            logger.warn("pupupContents of the annotation is: {}",popupContentsTemp);
            String highlightedTextTemp = extractedAnnotationProperties.highlightedText ?: '';
            logger.warn("highlightedText of the annotation is: {}",highlightedTextTemp);
            annotationKey = "generatedKey-" + checksum(popupContentsTemp+highlightedTextTemp).toString();
            logger.warn("generated annotationKey {} by hashing pupupContents + highlightedText",annotationKey);
            extractedAnnotationProperties.uniqueName = annotationKey;
            logger.warn("inserted generated annotationKey as a uniqueName into data structure");

        }
        // this is for testing - remove later (probably some key got reginstered and does not allow to proceed)
        logger.warn("Processing annotation with annotationKey {}",annotationKey)
        extractedAnnotationProperties.uniqueName = annotationKey;
        logger.warn("Processing annotation with key {}", annotationKey)
        Vertex annotationVertex 
        List<Vertex> duplicateVertexList= g.query().has('entry','pdfAnnotation').has('annotationKey',annotationKey).vertices().toList();
        if (!duplicateVertexList.isEmpty()) {
          logger.warn("duplicateVertexList is not empty: {}",duplicateVertexList.toString());
          logger.warn("AnnotationKey {} already exists in the SpaceGraph", annotationKey);
          Vertex duplicateVertex = duplicateVertexList[0];
          logger.warn("At vertex", duplicateVertex.toString());
          GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
          String relativeFilePathDuplicate = (String) pipe.start(duplicateVertex).inE('isAnnotated').outV().next().getProperty('relativeFilePath');
          logger.warn("got relativeFilePathDuplicate {}", relativeFilePathDuplicate);
          fileVertex.setProperty("annotationExtractionError","duplicate file of : ".concat(relativeFilePathDuplicate));
          logger.warn("Flaged fileVertex {} as duplicate", fileVertex.toString());
        } else {
          GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
          List<Vertex> annotationVertexList = (List<Vertex>) pipe.start(fileVertex).outE('hasAnnotation').inV().has("annotationKey",annotationKey).toList()
          if ( ! annotationVertexList.isEmpty()) {
            annotationVertex = annotationVertexList[0]
            logger.warn('Retrieved existing annotationVertex {}', annotationVertex.toString())
          } else {

            annotationVertex = g.addVertex(null)
            annotationVertex.setProperty("annotationKey",annotationKey)
            annotationVertex.setProperty("entry","pdfAnnotation")
            annotationVertex.setProperty("creationDate",new Date(0))
            annotationVertex.setProperty("modificationDate",new Date(0))
            logger.warn('Created new annotationVertex {}', annotationVertex.toString())
            g.addEdge(null,fileVertex,annotationVertex,"isAnnotated");
            logger.warn('Linked annotationVertex {} to fileVertex {}',annotationVertex.toString(), fileVertex.toString());
          }
          g.commit()
          Date modificationDateOnSpaceGraph = (Date) annotationVertex.getProperty("modificationDate")
          Date modificationDateOnPDFFile = new Date(new Integer((String) extractedAnnotationProperties.modificationDate).longValue() * 1000L )
          if (annotationVertex.getProperty("creationDate") == 0) {
              Edge edge = g.addEdge(null,fileVertex,annotationVertex,"hasAnnotation")
              g.commit()
              logger.warn("AnnotationVertex {} is newly created, therefore added a link to file {}", annotationVertex.toString(), edge)
          }
          if (modificationDateOnSpaceGraph < modificationDateOnPDFFile ) {
              logger.warn("modificationDateOnSpaceGraph {} < modificationDateOnPDFFile {} therefore updating.", modificationDateOnSpaceGraph, modificationDateOnPDFFile)
              extractedAnnotationProperties.each {propertyKey, propertyValue ->
                switch (propertyKey) {
                  case 'rectangle':
                    logger.warn("Found propertyKey {} with value {}", propertyKey, propertyValue)
                    propertyValue.each { coordinateKey, coordinateValue ->
                        annotationVertex.setProperty(((String) propertyKey)+'-'+ ((String) coordinateKey),coordinateValue)
                        logger.warn('Added annotation property {}:{} with  to annotationVertex {}', ((String)propertyKey)+'-'+((String) coordinateKey), coordinateValue, annotationVertex,toString())
                    }
                    break;
                  case ['creationDate', 'modificationDate']:
                    logger.warn("Found propertyKey {} with value {}", propertyKey, propertyValue)
                    def convertedToDate = new Date(new Integer((String)propertyValue).longValue() * 1000L )
                    annotationVertex.setProperty((String) propertyKey,convertedToDate)
                    logger.warn('Added annotation property {}:{} with  to annotationVertex {}', propertyKey, convertedToDate, annotationVertex,toString())
                    break;
                  default:
                    logger.warn("Found propertyKey {} with value {}", propertyKey, propertyValue)
                    annotationVertex.setProperty((String) propertyKey,propertyValue)
                    logger.warn('Added annotation property {}:{} with  to annotationVertex {}', propertyKey, propertyValue, annotationVertex,toString())
                    break;
                }
              }
              g.commit()
          } else {
              logger.warn("AnnotationVertex {} was not modified since {} therefore not updating.", annotationVertex.toString(), extractedAnnotationProperties.modificationDate)
          }
       }
    }
  }
  /*
  * The functionality for which this method is needed is not yet implemented.
  */
  @TypeChecked
  static connectRelatedAnnotations(Vertex fileVertex) {
    logger.warn('Connecting related annotations on file {}', fileVertex.getProperty("fileName"))
    assertNotNull(fileVertex)
    def highlightAnnotationsByPage = [:]
    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
    def pagePipeFunction =  new PipeFunction<Vertex,Integer>() {
      public Integer compute(Vertex vertex) {
          return vertex.getProperty("page")
      }
    }
    def identityPipeFunction = new PipeFunction<Vertex,Vertex>() {
      public Vertex compute(Vertex vertex) {
          return vertex
      }
    }
    pipe.start(fileVertex).outE('hasAnnotation').inV().has('type','highlight').groupBy(highlightAnnotationsByPage,pagePipeFunction,identityPipeFunction).iterate()
    // original query -- just in case
    //fileVertex.outE('hasAnnotation').inV.has('type','highlight').groupBy(highlightAnnotationsByPage){it.page}{it}.iterate()
    logger.warn('Constructed a map of highlight annotations by page {}', highlightAnnotationsByPage)
    def textAnnotationsByPage = [:]
    pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(fileVertex).outE('hasAnnotation').inV().has('type','text').groupBy(highlightAnnotationsByPage,pagePipeFunction,identityPipeFunction).iterate()
    // original query -- just in case
    //fileVertex.outE('hasAnnotation').inV.has('type','text').groupBy(textAnnotationsByPage){it.page}{it}.iterate()
    logger.warn('Constructed a map of text annotations by page {}', textAnnotationsByPage)

    highlightAnnotationsByPage.each {page, annotations ->
      annotations.each {
          // continue here
      }
    }
  }

  @TypeChecked
  static String checksum( String input ) {
      def digest = java.security.MessageDigest.getInstance("MD5")
      digest.update( input.bytes )
      def checksum = new BigInteger(1,digest.digest()).toString(16).padLeft(32, '0')
      logger.warn("Calculaded MD checksum {} for string {}", checksum, input)
      return checksum
  }

}
