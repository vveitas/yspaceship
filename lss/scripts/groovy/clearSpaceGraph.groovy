@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.transform.TypeChecked
import groovy.json.JsonSlurper
import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.tinkerpop.gremlin.java.GremlinPipeline
import com.tinkerpop.blueprints.Vertex
import ySpaceShip

class clearSpaceGraph {

      static TitanGraph g = ySpaceShip.g
      static Logger logger
      static config = new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/configs/pbrain.conf"))

      @TypeChecked
      public static void main(String[] args) {
          // setting up logging stuff
          def configs = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
          PropertyConfigurator.configure(configs.toProperties())
          logger = LoggerFactory.getLogger('clearSpaceGraph.class');

          GremlinPipeline pipe = new GremlinPipeline();
          pipe.start(g).V().remove();
          g.commit();
          logger.warn("Cleared SpaceGraph by by the wish of the user...");

          def wholeGraphFile = System.getProperty("user.dir")+"/resources/spaceGraphDataLast.dat"
          new File(wholeGraphFile).write("{}");
          logger.warn("Cleared spaceGraphDataLast.dat file");

      }

}
