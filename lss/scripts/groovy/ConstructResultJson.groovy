@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.transform.TypeChecked
import groovy.lang.GroovyClassLoader
import groovy.json.JsonOutput
import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.tinkerpop.gremlin.groovy.Gremlin
import com.thinkaurelius.titan.core.TitanVertex
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.gremlin.java.GremlinPipeline

import static org.junit.Assert.assertNotNull

class ConstructResultJson {
      static Logger logger

      @TypeChecked
      static String constructResultJson(HashSet<Vertex> matchingVertexes) {
        long start = System.currentTimeMillis()
        
        // setting up logging stuff
        def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
        PropertyConfigurator.configure(config.toProperties())
        this.logger = LoggerFactory.getLogger('ConstructResultJson.class');
        // continue constructing JSON object according to this:
        // http://docs.oracle.com/javaee/7/api/javax/json/JsonObjectBuilder.html

        def json = [ matchingAuthors: []
                  , matchingPublications: []
                  , matchingTags: []
                  , matchingPDFfiles: []
                  , matchingAnnotations: []
                  ]
        matchingVertexes.each{ mVertex ->
            logger.warn("The matchingVertex is of type {}", mVertex.getProperty("entry"))

            switch (mVertex.getProperty("entry")) {
                case "person":
                    json.matchingAuthors.add([
                        id: mVertex.getId(),
                        firstName: mVertex.getProperty("firstName"),
                        lastName: mVertex.getProperty("lastName")
                    ])
                    break;
                case "publication":
                    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
                    int numberOfAnnotations = (int) pipe.start(mVertex).outE('pdfAttachment').inV().outE('isAnnotated').count()
                    logger.warn("got numberOfAnnotations {}", numberOfAnnotations);

                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    List fileVertex = pipe.start(mVertex).outE('pdfAttachment').inV().toList();
                    logger.warn("got fileVertex list {}", assertNotNull(fileVertex));

                    json.matchingPublications.add([
                        id: mVertex.getId(),
                        date: mVertex.getProperty("date"),
                        itemType: mVertex.getProperty("itemType"),
                        title: mVertex.getProperty("title"),
                        authorstext: mVertex.getProperty("authorstext"),
                        bibtexKey: mVertex.getProperty("bibtex_key"),
                        key: mVertex.getProperty("key"),
                        tagVertexIds: mVertex.getProperty("tagVertexIds"),
                        dateAdded: mVertex.getProperty("dateAdded"),
                        dateModified: mVertex.getProperty("dateModified"),
                        publisher: mVertex.getProperty("publisher"),
                        fileAttached: ! fileVertex.isEmpty(),
                        file: mVertex.getProperty("file"),
                        date: mVertex.getProperty("date"),
                        relativeFilePath:  fileVertex[0] ? fileVertex[0].getProperty("relativeFilePath") : "NoAttachment",
                        numberOfAnnotations: numberOfAnnotations
                    ])
                    break;
                case "tag":
                    json.matchingTags.add([
                        id: mVertex.getId(),
                        tag: mVertex.getProperty("tag")
                    ])
                    break;
                case "PDFfile":
                    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
                    List attachedPublications = pipe.start(mVertex).inE('pdfAttachment').toList()
                    logger.warn("Got attachedPublications list {}", assertNotNull(attachedPublications));

                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    int numberOfAnnotations = (int) pipe.start(mVertex).outE('isAnnotated').count()
                    logger.warn("got numberOfAnnotations {}", numberOfAnnotations);

                    json.matchingPDFfiles.add([
                        id: mVertex.getId(),
                        fileName: mVertex.getProperty("fileName"),
                        relativeFilePath: mVertex.getProperty("relativeFilePath"),
                        fileNameHash: mVertex.getProperty("fileNameHash"),
                        publicationAttached: ! attachedPublications.isEmpty(),
                        numberOfAnnotations: numberOfAnnotations
                    ])
                    break;
                case "pdfAnnotation":
                    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
                    def file = pipe.start(mVertex).inE('isAnnotated').outV().next(1)[0].getProperty("relativeFilePath");
                    logger.warn("got file relativePath {}", file);

                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    def publication;
                    def bibtexKey;
                    def pdfFileID;
                    def publicationID;
                    def interimPipe = pipe.start(mVertex).inE('isAnnotated').outV().inE('pdfAttachment').outV()
                    if (interimPipe.hasNext()) {
                        def pVertex = interimPipe.next(1)[0]
                        publication = (String) pVertex.getProperty("authorstext")+" ("+(String) pVertex.getProperty("date")+"). "+(String) pVertex.getProperty("title");
                        bibtexKey = pVertex.getProperty("bibtex_key");
                        publicationID = pVertex.getId();
                    } else {
                        pipe = new GremlinPipeline<Vertex,Vertex>();
                        def fVertex = pipe.start(mVertex).inE('isAnnotated').outV().next(1)[0];
                        publication = fVertex.getProperty("fileName");
                        pdfFileID = fVertex.getId();
                    }
                    logger.warn("got publication title {}", publication);
                    def publicationHash = checksum(publication.toString());
                    logger.warn("calculated publicationHash {}", publicationHash);
                    logger.warn("got bibtexKey {}", bibtexKey);
                    logger.warn("got publicationID {}", publicationID);
                    logger.warn("got pdfFileID {}", pdfFileID);

                    switch (mVertex.getProperty("type")) {
                      case "text":
                          json.matchingAnnotations.add([
                              id: mVertex.getId(),
                              author: mVertex.getProperty("author"),
                              uniqueName: mVertex.getProperty("uniqueName"),
                              displayText: mVertex.getProperty("popupContents"),
                              type: mVertex.getProperty("type"),
                              subType: mVertex.getProperty("subType"),
                              date: mVertex.getProperty("modificationDate"),
                              dateAdded: mVertex.getProperty("creationDate"),
                              publication: publication,
                              publicationHash: publicationHash,
                              publicationID: publicationID,
                              pdfFileID: pdfFileID,
                              relativeFilePath: file,
                              page: (int) mVertex.getProperty("page")+1,
                              topLeftX: mVertex.getProperty("rectangle-topLeftX"),
                              topRightX: mVertex.getProperty("rectangle-topRightX"),
                              topLeftY: mVertex.getProperty("rectangle-topLeftY"),
                              bibtexKey: bibtexKey

                          ])
                          break;
                      case "highlight":
                          json.matchingAnnotations.add([
                              id: mVertex.getId(),
                              author: mVertex.getProperty("author"),
                              uniqueName: mVertex.getProperty("uniqueName"),
                              displayText: mVertex.getProperty("highlightedText"),
                              type: mVertex.getProperty("type"),
                              subType: mVertex.getProperty("subType"),
                              date: mVertex.getProperty("modificationDate"),
                              dateAdded: mVertex.getProperty("creationDate"),
                              publication: publication,
                              publicationHash: publicationHash,
                              publicationID: publicationID,
                              pdfFileID: pdfFileID,
                              relativeFilePath: file,
                              page: (int) mVertex.getProperty("page")+1,
                              topLeftX: mVertex.getProperty("rectangle-topLeftX"),
                              topRightX: mVertex.getProperty("rectangle-topRightX"),
                              topLeftY: mVertex.getProperty("rectangle-topLeftY"),
                              bibtexKey: bibtexKey
                          ])
                          break;
                    }
                    break;
            }
        }

        json.matchingAnnotations = sortByCoordinates(json.matchingAnnotations);
        json.matchingAuthors = sortByLastName(json.matchingAuthors);
        json.matchingTags = sortAlphabetically(json.matchingTags);
        json.publicationsWithAnnotations = publicationsWithAnnotations(json.matchingAnnotations);
        
        def result = JsonOutput.toJson(json)
        //logger.warn("Result of the query: {}",result)

        long finish = System.currentTimeMillis()
        logger.warn("constructResultJson method finished in {} seconds",(finish-start)/1000)

        return result

      }

      static List<Map> sortByCoordinates(List<Map> annotationList) {
        annotationList.sort { a,b ->
          a.publication <=> b.publication ?:
          a.page <=> b.page ?: 
          a.topRightX <=> b.topRightX ?:
          a.topLeftY <=> b.topLeftY 
          

        }
        logger.warn("Sorted annotationList by publication, page and coordinates")
        return annotationList;
      }

      static List<Map> sortByLastName(List<Map> authorsList) {
        authorsList.sort { a,b ->
          a.lastName <=> b.lastName 
        }
        logger.warn("Sorted matchingAuthors by lastName")
        return authorsList;
      }

      static List<Map> sortAlphabetically(List<Map> tagsList) {
        tagsList.sort { a,b ->
          a.tag <=> b.tag
        }
        logger.warn("Sorted matchingTags alphabetically")
        return tagsList;
      }


      static List<Object> publicationsWithAnnotations(List<Map> annotationsList) {
        def publicationsWithAnnotations = [:]
        annotationsList.each{ annotation ->
          if (!publicationsWithAnnotations[annotation.publicationHash]){
            publicationsWithAnnotations[annotation.publicationHash]=[:]
            publicationsWithAnnotations[annotation.publicationHash]['title'] = annotation.publication
            publicationsWithAnnotations[annotation.publicationHash]['hash'] = annotation.publicationHash
            publicationsWithAnnotations[annotation.publicationHash]['annotations'] = [];

          } 
          publicationsWithAnnotations[annotation.publicationHash]['annotations'].add(annotation)
          logger.warn("Added annotation {} to publication {} with publicationHash {}",annotation.uniqueName,annotation.publication,annotation.publicationHash)
        }
        logger.warn("Added {} annotations to {} publications",annotationsList.size(),publicationsWithAnnotations.size());
        logger.warn("publicationsWithAnnotations: {}",JsonOutput.toJson(publicationsWithAnnotations))
        return publicationsWithAnnotations.collect{it.value};
      }

      @TypeChecked
      static String checksum( String input ) {
        def digest = java.security.MessageDigest.getInstance("MD5")
        digest.update( input.bytes )
        def checksum = new BigInteger(1,digest.digest()).toString(16).padLeft(32, '0')
        logger.warn("Calculated MD checksum {} for string {}", checksum, input)
        return checksum
      }


}
