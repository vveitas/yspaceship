@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='commons-io', module='commons-io', version='2.4')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked

// imports a groovy class from file -- not sure this will work with hava too
import ySpaceShip
import SpaceGraph

import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import java.text.DecimalFormat;

import org.apache.commons.io.FilenameUtils

import com.tinkerpop.gremlin.java.GremlinPipeline
import com.tinkerpop.pipes.PipeFunction
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge

import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.log4j.*;

import static org.junit.Assert.assertNotNull

import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import org.apache.commons.io.IOUtils


class UpdatePublications {

  static SpaceGraph sg = ySpaceShip.sg
  static TitanGraph g = sg.g 
  static Logger logger
  static Map config = new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/configs/pbrain.conf"))
  static Map zoteroFileAttachmentsMap = [:]
  static Object zoteroLibraryMetadata
  static Map allCollectionsVersions

  @TypeChecked
  static main(args) {

    // setting up logging stuff
    def configs = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
    PropertyConfigurator.configure(configs.toProperties())
    logger = LoggerFactory.getLogger('UpdatePublications.class');


    zoteroLibraryMetadata = getZoteroLibraryMetadata();
    assertNotNull(zoteroLibraryMetadata);
    allCollectionsVersions = (Map) getAllZoteroCollectionsVersions();
    assertNotNull(allCollectionsVersions);
    def myLibrary = false; // a variable to indicate if we will be looking at top level "My Library" collection -- metadata of which is difficult to access via Zotero API..

    List<String> collectionsArray = [];
    if (this.config.zotero_collectionID != "") {
      collectionsArray.add((String)config.zotero_collectionID);
      logger.warn("{} collection is specified in the configuration file", collectionsArray.toString());
      collectionsArray = getZoteroSubCollectionsOf(collectionsArray, (String) config.zotero_collectionID);
    } else {
      logger.warn("Configuration does not specify any collection number, therefore extracting all")
      // collectionsArray.add("MyLibrary");
      myLibrary = true; // so that we use collectionsArray to estimate total number of items in the library, but then working only on top level...
      allCollectionsVersions.each {key, value ->
          collectionsArray.add((String) key);
      }
    }

    double numberOfItemsInLibrary = (double) getNumberOfItemsInCollections(collectionsArray);
    if (myLibrary) {
        collectionsArray = ["MyLibrary"];
    }
    logger.warn("Retrieved {} collections to process..", collectionsArray)

    double processedItems = 0d;
    double numberOfUpdatedPublications = 0d;

    collectionsArray.each { String collectionID ->
      logger.warn("Processing collection {}:", collectionID);
      List<Map> zoteroEntries = (List<Map>) getPublicationsFromCollection((String) collectionID)
      //def attachments = getAttachmentsFromZotero(zoteroEntries);
      def attachments = getAttachmentsFromZotero((String) collectionID);
      zoteroFileAttachmentsMap = getAttachedFilesMap(attachments);
      def zoteroCollectionVersion = getZoteroCollectionVersion((String) collectionID) ?: -1;
      def spaceGraphVersion = getSpaceGraphCollectionVersion((String) collectionID)

      if (zoteroCollectionVersion == spaceGraphVersion) {
        logger.warn("Colection's {} versions on spaceGraph {} and Zotero {} match: not updating.", collectionID, spaceGraphVersion, zoteroCollectionVersion)
      } else {
        logger.warn("Collection {} is older on SpaceGraph {} than on Zotero {}: updating.", collectionID, spaceGraphVersion, zoteroCollectionVersion)

        sg.emitProgress('zotero-synchronization',(double) processedItems/numberOfItemsInLibrary);

        logger.warn("Updating SpaceGraph with collection {}:", collectionID)

        zoteroEntries.each { Map item ->
          Map entry = (Map) item.data
          if (checkItemType(entry)) {
            logger.warn("Entry type is {} therefore updating creating publication in SpaceGraph", entry.itemType);
            Vertex publicationVertex = getPublicationVertexFromSpaceGraph((String) entry.key)
            if (publicationVertex.getProperty("version") == null || (int) publicationVertex.getProperty("version") < (int) entry.version) {
              logger.warn("Publication {} version in SpaceGraph {} is older than in Zotero {}, therefore updating", publicationVertex.getProperty("title"), publicationVertex.getProperty("version"),entry.version)
              updatePublicationVertexInSpaceGraph(publicationVertex, entry)
              numberOfUpdatedPublications+=1
            } else {
              logger.warn("Publication {} has matching versions in SpaceGraph ({}) and in Zotero ({}), therefore not updating", publicationVertex.toString(), publicationVertex.getProperty("version"), entry.version)
            }
          }
          processedItems += 1d
          sg.emitProgress('zotero-synchronization', (double) processedItems /  numberOfItemsInLibrary);
        }

      }
      g.query().has('type','global').vertices().iterator().next().setProperty((String) collectionID, (String) zoteroCollectionVersion)
      g.commit()
      logger.warn("Updated collection version in SpaceGraph to {}", zoteroCollectionVersion);
    }
    logger.warn("Done updating SpaceGraph with {} publications", numberOfUpdatedPublications)
    sg.emitProgress('zotero-synchronization',1d);

  }

  @TypeChecked
  static List getZoteroSubCollectionsOf(List subCollectionsArray, String collectionID) {
    logger.warn("Getting Zotero subcollections of collection {}", collectionID)
    String query = 'https://api.zotero.org/users/' +
                this.config.zotero_userID+
                '/collections/'+collectionID+
                '/collections/?format=versions&key='+
                this.config.zotero_key;
    logger.warn("Executing query {} ",query)
    String curl_query = """ curl -H Zotero-API-version: 3 ${query} """

    def proc = curl_query.execute()
    def inputStream = proc.getInputStream()
    def response = IOUtils.toString(inputStream, "UTF-8");
    //proc.waitFor()
    //def response = proc.in.text
    logger.warn("Received response {} ",assertNotNull(response))
    Map subCollectionsVersions = (Map) new JsonSlurper().parseText(response)
    subCollectionsVersions.each { key,value ->
      subCollectionsArray.add(key);
      logger.warn("Checking if there are any subcollections in collection {}", collectionID)
      subCollectionsArray = getZoteroSubCollectionsOf(subCollectionsArray, (String) key);
    }
    logger.warn("subCollectionsArray is {}", assertNotNull(subCollectionsArray))
    return subCollectionsArray;

  }

  @TypeChecked
  static boolean checkItemType(Map entry) {
      boolean publication = true;
      def itemType = entry.itemType;
      if (itemType == 'note' | itemType == 'attachment') {
        publication = false;
      }
      return publication;
  }

  @TypeChecked
  static Object getZoteroLibraryMetadata() {
    String query = 'https://api.zotero.org/users/' +
                this.config.zotero_userID+
                '/collections/?key='+
                this.config.zotero_key;
    logger.warn("Executing query {} ",query)
    String curl_query = """ curl -H Zotero-API-version: 3 ${query} """

    def proc = curl_query.execute()
    def inputStream = proc.getInputStream()
    def response = IOUtils.toString(inputStream, "UTF-8");

    //proc.waitFor()
    //def response = proc.in.text
    logger.warn("Received response {} ",response)
    Object collectionMetadata =  new JsonSlurper().parseText(response)
    return collectionMetadata;
  }

  @TypeChecked
  static int getNumberOfItemsInCollections(List allCollectionsIDs) {
      logger.warn("Getting the number of items in specified collections {}", allCollectionsIDs);
      def numberOfItemsInLibrary = 0
      zoteroLibraryMetadata.each { Map collectionMetadata ->
        if (allCollectionsIDs.contains(collectionMetadata.key)) {
          int numberOfItemsInCollection = (int) ((Map) collectionMetadata.meta).numItems;
          numberOfItemsInLibrary += numberOfItemsInCollection;
          logger.warn("Number of items in Zotero collection {} is {}", collectionMetadata.key, numberOfItemsInCollection)
        } else {
          logger.warn("Found collection {} which is not specified in configuration file; skipping", collectionMetadata.key)
        }
      }
      logger.warn("Total number of items in the library (configured collections for extraction) is {}", numberOfItemsInLibrary);
      return numberOfItemsInLibrary
  }

  @TypeChecked
  static Object getAllZoteroCollectionsVersions() {
    logger.warn("Getting metadata about versions of all collections in Zotero database.")
    String query = 'https://api.zotero.org/users/' +
                this.config.zotero_userID+
                '/collections/?format=versions&key='+
                this.config.zotero_key;
    logger.warn("Executing query {} ",query)
    String curl_query = """ curl -H Zotero-API-version: 3 ${query} """

    def proc = curl_query.execute()
    proc.waitFor()
    def response = proc.in.text
    logger.warn("Received response {} ",assertNotNull(response));
    Object zoteroCollectionVersion =  new JsonSlurper().parseText(response);
    assertNotNull(zoteroCollectionVersion);
    return zoteroCollectionVersion;
  }

  @TypeChecked
  static getZoteroCollectionVersion(String collectionID) {
      logger.warn("Getting version of collectionID {} from allCollectionsVersions {}",collectionID,allCollectionsVersions);
      assertNotNull(allCollectionsVersions);
      def zoteroCollectionVersion = allCollectionsVersions.get(collectionID)
      logger.warn("Zotero collection's {} version is: {}", collectionID, zoteroCollectionVersion)
      return zoteroCollectionVersion
  }

  @TypeChecked
  static int getSpaceGraphCollectionVersion(String collectionID) {
    def spaceGraphGlobal = g.query().has('type','global').vertices().iterator().next()
    int spaceGraphVersion;
    if (spaceGraphGlobal.hasProperty(collectionID)) {
        spaceGraphVersion = (int) spaceGraphGlobal.getProperty(collectionID);
        logger.warn("Got collection's {} version on SpaceGraph: {}",collectionID, spaceGraphVersion)
    } else {
        logger.warn("Collection {} version does not exist on SpaceGraph: creating.", collectionID)
        spaceGraphVersion = 0;
        spaceGraphGlobal.setProperty(collectionID,spaceGraphVersion);
        g.commit();
    }

    logger.warn("Current collection's {} version on SpaceGraph: {}",collectionID, spaceGraphVersion)
    return spaceGraphVersion
  }

  @TypeChecked
  static getZoteroItemsInCollectionMap(String collectionID) {
    logger.warn("Getting all items in collection {} from Zotero", collectionID)
    String query = 'https://api.zotero.org/users/' +
                this.config.zotero_userID+
                '/collections/'+collectionID+
                '/items/?since=0&format=versions&key='+
                this.config.zotero_key;
    logger.warn("Executing query {} ",query)
    String curl_query = """ curl -H Zotero-API-version: 3 ${query} """

    def proc = curl_query.execute()
    def inputStream = proc.getInputStream()
    def response = IOUtils.toString(inputStream, "UTF-8");
    //def response = proc.in.text
    assertNotNull(response)
    //proc.waitFor()
    //def response = proc.in.text
    logger.warn("Received response {} ",assertNotNull(response))
    def allZoteroItems =  new JsonSlurper().parseText(response)
    logger.warn("Zotero items in collection {} are: {}", collectionID, allZoteroItems)
    assertNotNull(allZoteroItems)
    return allZoteroItems
  }

  @TypeChecked
  static Vertex getPublicationVertexFromSpaceGraph(String item_key) {
    logger.warn("Retrieving publication vertex with key {}", item_key)
    Vertex publicationVertex
    if (! g.query().has("key",item_key).vertices().iterator().hasNext()) {
        publicationVertex = (Vertex) g.addVertex(null)
        publicationVertex.setProperty("key",item_key)
        publicationVertex.setProperty("entry","publication")
    } else {
        publicationVertex = (Vertex) g.query().has("key",item_key).vertices().iterator().next()
    }
    g.commit()
    logger.warn("Publication vertex for key {} is {} with title -- {}",item_key, publicationVertex.toString(), publicationVertex.getProperty("title"));
    return publicationVertex
  }

  @TypeChecked
  static List getPublicationsFromCollection(String collectionID) {
      logger.warn("Getting publications for collection {}", collectionID)
      int numberOfItemsInCollection = 0; 
      zoteroLibraryMetadata.each { Map collectionMetadata ->
        if (collectionMetadata.key == collectionID ) {
          numberOfItemsInCollection = (int) ((Map) collectionMetadata.meta).numItems;
        }
      }
      logger.warn("Number of items in collection is {}", numberOfItemsInCollection)
      int step = 50;
      def zoteroPublications = [];
      0.step numberOfItemsInCollection, step, {int from ->
          int to = from+step < numberOfItemsInCollection ? from+step : numberOfItemsInCollection
          int limit = to - from;
          logger.warn("Retrieving publications from collection {} from {} to {} (limit {})",collectionID,from,to,limit);
          String query;
          if (collectionID == "MyLibrary") {
            query = 'https://api.zotero.org/users/' +
                        config.zotero_userID+
                        '/items?limit='+limit+'&start='+from+'&itemType=-attachment&format=json&key='+
                        config.zotero_key;
          } else {
            query = 'https://api.zotero.org/users/' +
                      config.zotero_userID+
                      '/collections/'+
                      collectionID+
                      '/items?limit='+limit+'&start='+from+'&itemType=-attachment&format=json&key='+
                      config.zotero_key;
          }
          String curl_query = """ curl -H Zotero-API-version: 3 ${query} """
          logger.warn("Executing query {}", curl_query)
          def proc = curl_query.execute()

          def inputStream = proc.getInputStream()
          def response = IOUtils.toString(inputStream, "UTF-8");
          //def response = proc.in.text
          assertNotNull(response)
          List publicationsInCollection = (List) new JsonSlurper().parseText(response)
          logger.warn('Number of publictions retreived in step {}',zoteroPublications.size())
          logger.warn("Retrieved publications: {}", assertNotNull(publicationsInCollection))
          zoteroPublications.addAll(publicationsInCollection);

      }
      logger.warn('Total number of publictions retreived from collection {}: {}', collectionID,zoteroPublications.size())
      return zoteroPublications
  }
  
  @TypeChecked
  static List getAttachmentsFromZotero(List<Map> zoteroEntries) {
      def zoteroAttachments = [];
      zoteroEntries.each {Map item ->
          Map entry = (Map) item.data
          logger.warn("Retrieving attachments of entry with key {}",entry.key);
          String query;
          query = 'https://api.zotero.org/users/' +
                      config.zotero_userID+
                      '/collections/'+
                      config.zotero_collectionID+
                      '/items/'+entry.key+'/children&format=json&key='+
                      config.zotero_key;
          String curl_query = """ curl -H Zotero-API-version: 3 ${query} """
          logger.warn("Executing query {}", curl_query)
          def proc = curl_query.execute()

          def inputStream = proc.getInputStream()
          def response = IOUtils.toString(inputStream, "UTF-8");
          assertNotNull(response)
          List attachmentsOfEntry = (List<Map>) new JsonSlurper().parseText(response)
          logger.warn("Number of all attachments retreived for entryKey {}: {}",entry.key, attachmentsOfEntry.size())

          def pdfAttachments = []
          attachmentsOfEntry.each{ Map attachment ->
            Map data = (Map) item.data
            if (entry.contentType=="application/pdf") {
              pdfAttachments.add(entry);
            }
          }
          logger.warn("Retrieved attachments {} for entryKey {}", pdfAttachments, entry.key)
          zoteroAttachments.addAll(pdfAttachments);
    }

       logger.warn('Total number of attachments retreived from Zotero: {}',zoteroAttachments.size())

       // for debugging - saving to the file;
       def zoteroAttachmentsFile = System.getProperty("user.dir")+"/resources/zoteroAttachments.dat"
       new File(zoteroAttachmentsFile).write(JsonOutput.toJson(zoteroAttachments))
       logger.warn('Wrote Zotero attachments to file {}',assertNotNull(zoteroAttachmentsFile))

       return zoteroAttachments
  }

  @TypeChecked
  static List getAttachmentsFromZotero(String collectionID) {
      int numberOfItemsInCollection = 0; 
      zoteroLibraryMetadata.each { Map collectionMetadata ->
        if (collectionMetadata.key == collectionID ) {
          numberOfItemsInCollection = (int) ((Map) collectionMetadata.meta).numItems;
        }
      }
      logger.warn("Number of items in collection is {}", numberOfItemsInCollection)
      def zoteroAttachments = [];
      int from = 0;
      int step = 50;
      boolean itemsExist = true;
      while (itemsExist) {
          int to = from+step
          logger.warn("Retrieving attachments from collection {} from {} to {} (step {})",collectionID,from,to,step);
          String query;
          if (collectionID == "MyLibrary") {
            query = 'https://api.zotero.org/users/' +
                        config.zotero_userID+
                        '/items?limit='+step+'&start='+from+'&itemType=attachment&format=json&key='+
                        config.zotero_key;
          } else {
            query = 'https://api.zotero.org/users/' +
                      config.zotero_userID+
                      '/collections/'+
                      collectionID+
                      '/items?limit='+step+'&start='+from+'&itemType=attachment&format=json&key='+
                      config.zotero_key;
          }
          String curl_query = """ curl -H Zotero-API-version: 3 ${query} """
          logger.warn("Executing query {}", curl_query)
          def proc = curl_query.execute()

          def inputStream = proc.getInputStream()
          def response = IOUtils.toString(inputStream, "UTF-8");
          assertNotNull(response)
          List attachmentsInCollection = (List) new JsonSlurper().parseText(response)
          if (attachmentsInCollection.isEmpty()) {
            itemsExist = false;
            logger.warn("Zotero attachments query returned no items -- stopping and returning.")
          } else {
            logger.warn('Number of attachments retreived in step {}',attachmentsInCollection.size())
            logger.warn("Retrieved attachments: {}", assertNotNull(attachmentsInCollection))
            zoteroAttachments.addAll(attachmentsInCollection);
            from = from +step;
          }
      }

      logger.warn('Total number of attachments retreived from Zotero: {}',zoteroAttachments.size())

       // for debugging - saving to the file;
       def zoteroAttachmentsFile = System.getProperty("user.dir")+"/resources/zoteroAttachments.dat"
       new File(zoteroAttachmentsFile).write(JsonOutput.toJson(zoteroAttachments))
       logger.warn('Wrote Zotero attachments to file {}',assertNotNull(zoteroAttachmentsFile))

       return zoteroAttachments
  }

  @TypeChecked
  static Map getAttachedFilesMap(Object zoteroAttachments) {
      logger.warn("Constructing a map of file attachments.")
      Map fileAttachmentsMap = [:]
      zoteroAttachments.each { Map entry ->
          String zotero_key = ((Map) entry.data).parentItem
          String file_path = ((Map) entry.data).path
          logger.warn('Retrieved file path {} for attachment to publication with zotero_key {}', file_path, zotero_key)
          def file_name = new FilenameUtils().getName(file_path);
          // zotero started to add word attachments: in the begining for newer file attachments - removing that...
          def patternToRemove = ~/(^attachments:)/
          if (file_name!=null){ file_name= file_name.minus(patternToRemove) }

          fileAttachmentsMap[zotero_key] = file_name
          logger.warn("Added file attachment {} to publication with zotero_key {}", file_name, zotero_key)
      }
      return fileAttachmentsMap
  }

  @TypeChecked
  static getBibtexKey(String zoteroItemKey) {
      logger.warn("Getting bibtex entry for zoteroItemKey {}", zoteroItemKey)
      String query = 'https://api.zotero.org/users/' +
                      config.zotero_userID+
                      '/items/'+ zoteroItemKey +
                      '?format=bibtex&key='+
                      config.zotero_key;
      String curl_query = """ curl -H Zotero-API-version: 3 ${query} """
      logger.warn("Executing the query: {}", curl_query)
      def proc = curl_query.execute()
      def inputStream = proc.getInputStream()
      def bibtexEntry = IOUtils.toString(inputStream, "UTF-8");
      //proc.waitFor()
      //def bibtexEntry = proc.in.text
      logger.warn("Retrieved bibtexEntry {} for zotero_key {}", bibtexEntry, zoteroItemKey)
      String bibtexKey

      if (bibtexEntry != null) {
          def start = bibtexEntry.indexOf("{") + 1
          def finish = bibtexEntry.indexOf(",")
          bibtexKey=bibtexEntry.substring(start,finish)
      } else {
          logger.warn("There was an error getting a bibtex key for the publication {}", zoteroItemKey)
      }
      logger.warn("Retrieved bibtexKey {} for zotero_key {}", bibtexKey, zoteroItemKey)
      return bibtexKey
  }

  /*
  static void updateSinglePublication(String publicationVertexId) {


    assertNotNull(publicationVertexId)
    logger.warn("updating single publicationVertex, id {}", publicationVertexId)
    def publicationVertex = g.v(publicationVertexId)
    def entry = getSinglePublicationFromZotero(publicationVertex.key)
    updatePublicationVertexInSpaceGraph(publicationVertex, entry);
    if (publicationVertex.file!="NoAttachment") {
        def fileNameHash = checksum(publicationVertex.file);
        def fileVertexArray = g.query().has('fileNameHash',fileNameHash).vertices().iterator().toList();
    }


    if (fileVertexArray.isEmtpy()) {

    }
    Edge edge = publicationVertex.outE("pdfAttachment").inV.filter{it.fileNameHash == fileNameHash}\
        ? publicationVertex.outE("pdfAttachment").inV.next()
        : this.g.addEdge(0, publicationVertex, person, "isAuthored")

    if (!fileVertexArray.isEmpty()){
        def fileVertex = fileVertexArray[0];
        logger.warn("File vertex for file {} exists -- retrieved {}", file, fileVertex.toString());

        g.addEdge(publicationVertex,fileVertex,'pdfAttachment')
        g.commit()
        logger.warn("Added link from publicationVertex {} to fileVertex {}.", publicationVertex.toString(), fileVertex.toString())
        logger.warn("i.e. publication titled {} is linked to file named {}.", publicationVertex.title, fileVertex.file)
    }

    def fullFilePath = config.pdf_library_dir.toString() + fileVertex.relativeFilePath
    def annotations = getPDFAnnotations(fileVertex, fullFilePath)
    updateFileAnnotationsInSpaceGraph(fileVertex, annotations);

  }

  static Object getSinglePublicationFromZotero(itemKey) {
      logger.warn("Getting publications for collection {}", collectionID)
      String query = 'https://api.zotero.org/users/' +
                  config.zotero_userID+
                  '/items/'+ itemKey+
                  '&key='+ config.zotero_key;
      String curl_query = """ curl -H Zotero-API-version: 3 ${query} """
      logger.warn("Executing query {}", curl_query)
      def proc = curl_query.execute()
      proc.waitFor()
      def response = proc.in.text
      assertNotNull(response)
      def zoteroPublications = new JsonSlurper().parseText(response)
      logger.warn("Retrieved publication from Zotero {}", zoteroPublications)
      return zoteroPublications[0]
  }
  */

  @TypeChecked
  static Vertex updatePublicationVertexInSpaceGraph(Vertex publicationVertex, Map entry) {
    assertNotNull(publicationVertex)
    assertNotNull(entry)
    logger.warn("updating pulicatonVertex {} with entry {}", publicationVertex.toString(),assertNotNull(entry));
    def title =""
    switch (entry.itemType) {
      case "email":
        title=entry.subject
        break;
      default:
        title=entry.title
        break;
    }
    publicationVertex.setProperty("title", title)
    logger.warn("Added/updated property title: {} to vertex {}", entry.title, publicationVertex.toString())
    publicationVertex.setProperty("version", entry.version)
    logger.warn("Added/updated property version: {} to vertex {}", entry.version, publicationVertex.toString())
    publicationVertex.setProperty("type", entry.itemType)
    logger.warn("Added/updated property itemType: {} to vertex {}", entry.itemType, publicationVertex.toString())
    g.commit()
    def authors = []
    def editors = []
    entry.each{ property->
      switch (property.key) {
         case "creators":
            property.value.each {Map creator->
                creator.creatorType == 'author'\
                    ?authors.add(((String) creator.firstName) + " " + ((String) creator.lastName))
                    :editors.add(((String) creator.firstName) + " " + ((String) creator.lastName))

                Vertex person
                if (! g.query().has("firstName", creator.firstName).has("lastName", creator.lastName).vertices().iterator().hasNext()) {
                    person=g.addVertex(null);
                    person.setProperty('entry','person');
                    person.setProperty('firstName',creator.firstName);
                    person.setProperty('lastName',creator.lastName);
                } else {
                    person =  g.query().has("firstName", creator.firstName).has("lastName", creator.lastName).vertices().iterator().next()
                }
                g.commit();
                logger.warn("Added/updated person: {}, {} to vertex {}", creator.lastName, creator.firstName, person.toString())

                GremlinPipeline pipe = new GremlinPipeline();
                def personPipeFunction =  new PipeFunction<Vertex,Boolean>() {
                  public Boolean compute(Vertex vertex) {
                    return vertex.getId() == person.getId();
                  }
                }
                List personVertexList =  (List) pipe.start(publicationVertex).outE('isAuthored').inV().filter(personPipeFunction).toList();
                logger.warn("retrieved personVertexList {} from publicationVertex {}", personVertexList, publicationVertex);
                Edge edge;
                if (!personVertexList.isEmpty()) {
                  pipe = new GremlinPipeline();
                  edge = (Edge) pipe.start(publicationVertex).outE('isAuthored').inV().filter(personPipeFunction).back(2).next();
                } else {
                  edge = (Edge) g.addEdge(null, publicationVertex, person, "isAuthored");
                }
                g.commit();
                logger.warn("Added/updated link from publication {} to person {}", publicationVertex.toString(), person.toString())
              }
            break
        case "tags":
            // creates new vertex for each tag/project and connects to the publication
            List<String> tagList = new ArrayList<String>()
            property.value.each { Map ttag ->
                Vertex tagVertex
                if (! g.query().has("entry","tag").has("tag", ttag.tag).vertices().iterator().hasNext() ) {
                    tagVertex = g.addVertex(null);
                    tagVertex.setProperty("entry","tag");
                    tagVertex.setProperty("tag",ttag.tag);
                } else {
                    tagVertex = g.query().has("entry","tag").has("tag", ttag.tag).vertices().iterator().next()
                }
                g.commit();
                logger.warn("Added/updated tag: {} on tagVertex {}", ttag.tag, tagVertex.toString())

                GremlinPipeline pipe = new GremlinPipeline();
                def tagPipeFunction =  new PipeFunction<Vertex,Boolean>() {
                  public Boolean compute(Vertex vertex) {
                    return vertex.getId() == tagVertex.getId();
                  }
                }
                List tagVertexList =  (List) pipe.start(publicationVertex).outE('isTagged').inV().filter(tagPipeFunction).toList();
                logger.warn("retrieved tagVertexList {} from publicationVertex {}", tagVertexList, publicationVertex);
                Edge edge;
                if (!tagVertexList.isEmpty()) {
                  pipe = new GremlinPipeline();
                  edge = (Edge) pipe.start(publicationVertex).outE('isTagged').inV().filter(tagPipeFunction).back(2).next();
                } else {
                  edge = (Edge) g.addEdge(null, publicationVertex, tagVertex, "isTagged");
                }
                logger.warn("Added/updated link from publication: {} to tag {}", publicationVertex.toString(), tagVertex.toString())
                g.commit()
            }
            break
        case "collections":
            break
        default:
            if (property.value instanceof String && property.value !="") {
              publicationVertex.setProperty((String) property.key, (String) property.value)
              logger.warn("Added/updated property {} with value {} to vertex {}", property.key, property.value, publicationVertex.toString())
              g.commit()
            }
            break
      }
    }

    publicationVertex.setProperty("authorslist", authors)
    String authorstext = ""
    for (def no=0;no<authors.size();no++) {
        authorstext = authorstext +  ", " + authors[no]
    }
    authorstext = authorstext - ", "
    publicationVertex.setProperty("authorstext", authorstext)
    logger.warn("Set 'authorstext' property on vertex {}", publicationVertex.toString())
    publicationVertex.setProperty("editorslist", editors)
    String editorstext = ""
    for (def no=0;no<editors.size();no++) {
        editorstext = editorstext  + ", " + editors[no]

    }
    editorstext = editorstext - ", "
    publicationVertex.setProperty("editorstext", editorstext)
    logger.warn("Set 'editorstext' property on vertex {}", publicationVertex.toString())
    logger.warn("Updated a publication {} on vertex {}", entry.title, publicationVertex.toString())

    String zotero_key = publicationVertex.getProperty("key")
    String bibtex_key = getBibtexKey(zotero_key)
    publicationVertex.setProperty("bibtex_key",bibtex_key)
    logger.warn("Added/updated bibtex_key {} to publicationVertex {}", bibtex_key, publicationVertex.toString())

    String file = (String) zoteroFileAttachmentsMap[zotero_key]
    if (file == null) {
      publicationVertex.setProperty("file", "NoAttachment");
      logger.warn("file attachment for publicationVertex {} is not found in zoteroFileAttachmentsMap",publicationVertex.toString());
    } else {
      publicationVertex.setProperty("file", file);
      String fileNameHash = checksum(file);
      List<Vertex> fileVertexArray = (List<Vertex>) g.query().has('fileNameHash',fileNameHash).vertices().iterator().toList();
      if (!fileVertexArray.isEmpty()){
          Vertex fileVertex = fileVertexArray[0];
          logger.warn("File vertex for file {} exists -- retrieved {}", file, fileVertex.toString());
          g.addEdge(null,publicationVertex,fileVertex,'pdfAttachment')
          g.commit()
          logger.warn("Added link from publicationVertex {} to fileVertex {}.", publicationVertex.toString(), fileVertex.toString())
          logger.warn("i.e. publication titled {} is linked to file named {}.", publicationVertex.getProperty("title"), fileVertex.getProperty("file"))
      }

    }
    g.commit();
    return publicationVertex
  }

  @TypeChecked
  static String checksum( String input ) {
      def digest = java.security.MessageDigest.getInstance("MD5")
      digest.update( input.bytes )
      def checksum = new BigInteger(1,digest.digest()).toString(16).padLeft(32, '0')
      logger.warn("Calculated MD checksum {} for string {}", checksum, input)
      return checksum
  }

}
