@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked

import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.util.logging.Log4j

import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.thinkaurelius.titan.core.TitanGraph

import com.tinkerpop.gremlin.java.GremlinPipeline
import com.tinkerpop.pipes.PipeFunction
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Direction

import ySpaceShip
import ConstructResultJson


class GetNonFilteredResults {
      static TitanGraph g = ySpaceShip.g      
      static Logger logger
      static HashSet graph

      @TypeChecked
      static main(args) {

        // setting up logging stuff
        def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
        PropertyConfigurator.configure(config.toProperties())
        this.logger = LoggerFactory.getLogger('getNonFilteredResults.class');
        // continue constructing JSON object according to this:
        // http://docs.oracle.com/javaee/7/api/javax/json/JsonObjectBuilder.html
        g.commit();
        graph = new HashSet()
        // retrieving and saving to file
        def finishedProcesses = 0;
        def numberOfProcesses = 5;
        SpaceGraph.emitProgress('loading-spacegraph',(double) finishedProcesses/numberOfProcesses);
        getAuthorsFromSpaceGraph();
        finishedProcesses+=1;
        SpaceGraph.emitProgress('loading-spacegraph',(double) finishedProcesses/numberOfProcesses);
        getPDFFilesFromSpaceGraph();
        finishedProcesses+=1;
        SpaceGraph.emitProgress('loading-spacegraph',(double) finishedProcesses/numberOfProcesses);
        getPublicationsFromSpaceGraph();
        finishedProcesses+=1;
        SpaceGraph.emitProgress('loading-spacegraph',(double) finishedProcesses/numberOfProcesses);
        getTagsFromSpaceGraph();
        finishedProcesses+=1;
        SpaceGraph.emitProgress('loading-spacegraph',(double) finishedProcesses/numberOfProcesses);
        def annotationsRetrieved = getAnnotationsFromSpaceGraph();
        finishedProcesses+=1;
        SpaceGraph.emitProgress('loading-spacegraph',(double) finishedProcesses/numberOfProcesses);
        def result = ConstructResultJson.constructResultJson(graph);
        /*
        if (!annotationsRetrieved) {
          def json = new JsonSlurper().parseText(result);
          logger.warn("annotationsRetrieved: {} therefore appending status message to matchingAnnotations field", annotationsRetrieved);
          json.matchingAnnotations.add([retrieved: false])
          result = JsonOutput.toJson(json);
        }
        */

        def wholeGraphFile = System.getProperty("user.dir")+"/resources/spaceGraphDataLast.dat"
        new File(wholeGraphFile).write(result.toString())

        print result
      }

      @TypeChecked
      static getAuthorsFromSpaceGraph() {
            logger.warn("Retrieving all authors from the Space Graph")
            g.query().has("entry","person").vertices().each { personVertex ->
                logger.warn("Retrieved personVertex {}", personVertex.toString())
                graph.add(personVertex);
            }
      }

      @TypeChecked
      static getPDFFilesFromSpaceGraph() {
            logger.warn("Retrieving files which have Zotero entries from the Space Graph")
            PipeFunction filterNoPdfAttachments =  new PipeFunction<Vertex,Boolean>() {
              public Boolean compute(Vertex vertex) {
                  return vertex.getEdges(Direction.IN,'pdfAttachment').iterator().hasNext()==false
              }
            }
            List<Vertex> allPdfFiles = (List<Vertex>) g.query().has('entry','PDFfile').vertices().toList();
            List<Vertex> pdfFiles = [];
            logger.warn("got allPdfFiles: {}", allPdfFiles)
            for (Vertex pdfFile: allPdfFiles) {
              GremlinPipeline pipe = new GremlinPipeline();
              logger.warn("processing vertex: {}", pdfFile)
              List v = pipe.start(pdfFile).filter(filterNoPdfAttachments).toList();
              if (!v.isEmpty()) { 
                Vertex pdfFileVertex = v[0];
                logger.warn("Retrieved standalone pdfFileVertex {} of file {}", pdfFileVertex.toString(), pdfFileVertex.getProperty("fileName"))
                graph.add(pdfFileVertex);
              }
            }
      }

      @TypeChecked
      static getPublicationsFromSpaceGraph() {
            logger.warn("Retrieving all publications from the Space Graph")
            g.query().has("entry","publication").vertices().each { publicationVertex ->
                logger.warn("Retrieved publicationVertex {}", publicationVertex.toString())
                graph.add(publicationVertex);
            }
      }

      @TypeChecked
      static getTagsFromSpaceGraph() {
            logger.warn("Retrieving all tags from the Space Graph")
            g.query().has("entry","tag").vertices().each { tagVertex ->
                logger.warn("Retrieved tagVertex {}", tagVertex.toString())
                graph.add(tagVertex);
            }
      }

      @TypeChecked
      static boolean getAnnotationsFromSpaceGraph() {
            boolean retrieved = false;
            logger.warn("Retrieving all annotations from the Space Graph");
            def annotations = new HashSet();
            g.query().has("entry","pdfAnnotation").vertices().each { annotationVertex ->
                logger.warn("Retrieved annotationVertex {}", annotationVertex.toString());
                annotations.add(annotationVertex);
            }
            if (annotations.size() <= 1000) {
                logger.warn("Total annotation number {} is less then 1000, therefore returning.",annotations.size());
                graph.addAll(annotations);
                retrieved = true;
            } else {
                logger.warn("Total annotatation number {} is more than 1000 - not returning, because the browser will choke", annotations.size());
            }
            return retrieved;
      }
}
