@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked
import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import com.thinkaurelius.titan.core.TitanGraph
import com.thinkaurelius.titan.core.TitanGraphQuery

import java.net.ServerSocket
import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory

public class ySpaceShip {

    static SpaceGraph sg = new SpaceGraph();
    public static TitanGraph g = sg.g;
    static Logger logger

    @TypeChecked
    public void ass() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
              sg.gracefulShutdown()
            }
          });
    }

    @TypeChecked
    public static void main(String[] args) {
        def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
        PropertyConfigurator.configure(config.toProperties())
        logger = LoggerFactory.getLogger('ySpaceShip.class');

        def server = new ServerSocket(3003)
        logger.warn('Started Socket server on port 3003')
        while(true) {
            server.accept { socket ->
                socket.withStreams { input, output ->
                    def reader = input.newReader()
                    def buffer = reader.readLine()
                    Map message = (Map) new JsonSlurper().parseText(buffer);
                    logger.warn("Received message: {}",message)
                    def classToRun = message.class;
                    def data = message.data;
                    switch (classToRun) {
                        case "ApplyPublicationsFilter":
                            logger.warn("Running applyPublicationsFilter with message: ",data);
                            new ApplyPublicationsFilter().run((Map) data);
                            break;
                        case "ApplyAnnotationsFilter":
                            new ApplyAnnotationsFilter().run((Map) data);
                            break;
                    }
                }
            }
        }

    }
}
