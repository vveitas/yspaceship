@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked

import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.gremlin.groovy.Gremlin

import java.text.DecimalFormat;

import groovy.json.JsonOutput

import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.log4j.*;

import static org.junit.Assert.assertNotNull

import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.thinkaurelius.titan.core.TitanType
import com.thinkaurelius.titan.core.TitanVertex


public class SpaceGraph {
  static {
    Gremlin.load()
  }

  TitanGraph g
  static Logger logger

  @TypeChecked
  public SpaceGraph() {
      long start = System.currentTimeMillis()

      // setting up logging stuff
      def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
      PropertyConfigurator.configure(config.toProperties())
      logger = LoggerFactory.getLogger('SpaceGraph.class');

      initSpaceGraph()

      long finish = System.currentTimeMillis()
      logger.warn("SpaceGraph static class initialize method finished in {} seconds",(finish-start)/1000)
  }

  @TypeChecked
  public initSpaceGraph() {
      long start = System.currentTimeMillis()
      def cassandra_config_dir = "file://" + System.getProperty("user.dir") + "/configs/cassandra.yaml"
      def elasticsearch_dir = System.getProperty("user.dir") + "/../spacegraph/es"

      Configuration conf = new BaseConfiguration();
      conf.setProperty("storage.backend","cassandra");
      conf.setProperty("storage.hostname","localhost");

      // remote configuration
      conf.setProperty("storage.index.search.backend","elasticsearch");
      conf.setProperty("storage.index.search.hostname","127.0.0.1");
      conf.setProperty("storage.index.search.client-only",true);

      // embedded configuration (does not work well from gremlin console)
      /*
      conf.setProperty("storage.index.search.backend","elasticsearch");
      conf.setProperty("storage.index.search.directory",elasticsearch_dir);
      conf.setProperty("storage.index.search.client-only",false);
      conf.setProperty("storage.index.search.local-mode",true);
      */

      g = TitanFactory.open(conf)
      assertNotNull(g)
      logger.warn("Initialized graph {}", g.toString())
      // checks if this is a virgin graph or not

      if (g.query().has('type','global').vertices().toList().size() == 0) {

        logger.warn("This is the first initialization of virgin graph.")

          if (g.getTypes(TitanType.class).toList().size() == 0) {
          createIndexes()
        }


        Vertex globalPropertiesVertex = g.addVertex(null)
        globalPropertiesVertex.setProperty("type","global")
        //globalPropertiesVertex.addProperty("version",0)
        g.commit()
        logger.warn("Added global properties vertex {}",globalPropertiesVertex.toString())
        logger.warn("g.getTypes(TitanType.class): {}",g.getTypes(TitanType.class))
        logger.warn("g.getTypes(TitanType.class).toList().size(): {}",g.getTypes(TitanType.class).toList().size())

      }
      long finish = System.currentTimeMillis()
      logger.warn("initSpaceGraph method finished in {} seconds",(finish-start)/1000)
  }

  def createIndexes() {

    long start = System.currentTimeMillis()
    // this will have to be adapted to the new structure of the graph
    // index edges by their creation dates
    // a good idea would be also indexing edges by their importance, but this is later probably.
    //def creationDate = this.g.makeKey('creationDate').dataType(Date.class).indexed(Vertex.class).unique().make()
    //def creationDate = this.g.makeType().name('creationDate').unique(Direction.OUT).dataType(Date.class).makePropertyKey()
    //this.g.makeLabel("isAssociated").sortKey(creationDate).make()
    //this.g.makeType().name("isAssociated").primaryKey(creationDate).makeEdgeLabel()

    // create graph with all data without indexes and then
    // get all labels by g.getTypes(TitanLabel.class)
    // get all types by g.getTypes(TitanKey.class)
    // or all of them together by g.getTypes(TitanTypes.class)
    // then manually crate indexes
    // and delete graph (the whole database) and recreate it from scrach, or rebuild indexes manually

    // index edge labels if needed (which is not clear..):
    /*
    g.baseGraph.makeLabel("isAuthored").make()
    g.baseGraph.makeLabel("isTagged").make()
    g.baseGraph.makeLabel("pdfAttachment").make()
    g.baseGraph.makeLabel("hasAnnotation").make()
    */

    // create local indexes for efficient traversals
    this.g.makeKey("lastName").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("firstName").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("itemType").dataType(String.class).indexed(Vertex.class).make()

    this.g.makeKey("bibtex_key").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("file").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("fileName").dataType(String.class).indexed(Vertex.class).unique().make()
    this.g.makeKey("fileNameHash").dataType(String.class).indexed(Vertex.class).unique().make()
    this.g.makeKey("fileModified").dataType(Date.class).indexed(Vertex.class).make()
    this.g.makeKey("annotationKey").dataType(String.class).indexed(Vertex.class).unique().make()
    this.g.makeKey("author").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("page").dataType(Integer.class).indexed(Vertex.class).make()
    this.g.makeKey("uniqueName").dataType(String.class).indexed(Vertex.class).unique().make()
    this.g.makeKey("modifiedDate").dataType(Date.class).indexed(Vertex.class).make()
    this.g.makeKey("version").dataType(Integer.class).indexed(Vertex.class).make()
    this.g.makeKey("key").dataType(String.class).indexed(Vertex.class).unique().make()
    this.g.makeKey("type").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("entry").dataType(String.class).indexed(Vertex.class).make()
    this.g.makeKey("tag").dataType(String.class).indexed(Vertex.class).unique().make()

    logger.warn('Created standard Titan indexes.')

    // create types for searching through elasticsearch
    this.g.makeKey("authorstext").dataType(String.class).indexed("search", Vertex.class).make()
    this.g.makeKey("editorstext").dataType(String.class).indexed("search", Vertex.class).make()
    this.g.makeKey("publicationTitle").dataType(String.class).indexed("search", Vertex.class).make();
    this.g.makeKey("title").dataType(String.class).indexed("search", Vertex.class).make();
    this.g.makeKey("popupContents").dataType(String.class).indexed("search", Vertex.class).make();
    this.g.makeKey("highlightedText").dataType(String.class).indexed("search", Vertex.class).make();
    //this.g.makeKey("tag").dataType(String.class).indexed("search", Vertex.class).make();
    //this.g.makeKey("firstName").dataType(String.class).indexed("search", Vertex.class).make();
    //this.g.makeKey("lastName").dataType(String.class).indexed("search", Vertex.class).make();

    logger.warn('Created indexes for elasticsearch backend.')

    long finish = System.currentTimeMillis()
    logger.warn("createIndexes finished in {} seconds",(finish-start)/1000)

    this.g.commit()
  }
  @TypeChecked
  public static void emitProgress(String name, Double completedPercents) {
      def pattern = "##.#"
      def percentForm = new DecimalFormat(pattern)
      def formattedString = percentForm.format(completedPercents*100);
      def json = [ name: name, completedPercents: formattedString ];
      def output = JsonOutput.toJson(json)
      System.err.print(output);
      logger.warn("Sent message {} to nodejs server",output);
      sleep(50);
  }

  @TypeChecked
  public static String normalizeString(String stringToNormalize) {
      logger.warn("Normalizing string {}", stringToNormalize)
      String normalizedString = stringToNormalize.replaceAll("[^a-zA-Z0-9.-]", "_");
      if (!stringToNormalize.equals(normalizedString)) {
        logger.warn("Found bad characters in string; replacing to {}",normalizedString)
      }
      return normalizedString;
  }

  @TypeChecked
  public void gracefulShutdown() {
    this.g.shutdown()
  }

}
