@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='commons-io', module='commons-io', version='2.4')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.lang.GroovyClassLoader
import groovy.json.JsonOutput
import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.tinkerpop.gremlin.groovy.Gremlin
import groovy.json.JsonSlurper
import org.apache.commons.io.FilenameUtils
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.log4j.*;
import static org.junit.Assert.assertNotNull
import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.tinkerpop.blueprints.impls.tg.TinkerGraph
import com.tinkerpop.blueprints.util.ElementHelper
import com.tinkerpop.pipes.PipeFunction
import java.io.InputStream
import java.lang.Long
import java.util.concurrent.TimeUnit
import SpaceGraph
import ConstructResultJson
import groovy.io.FileType
import com.tinkerpop.pipes.util.structures.Table
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.text.DecimalFormat;
import com.thinkaurelius.titan.core.TitanType
import com.thinkaurelius.titan.core.TitanVertex
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.gremlin.java.GremlinPipeline


class ImportClasses {

  static main(args) {
      def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
      PropertyConfigurator.configure(config.toProperties())
      def logger = LoggerFactory.getLogger('ImportClasses.class');

      logger.warn("Imported required classes with Graphes...");
  }
}
